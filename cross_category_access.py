#!/usr/bin/python3.6.0
# -*- coding: utf-8 -*-

from database_connection import DatabaseConnection
from category_access import CategoryAccess
from pprint import pprint

#abstract
class CrossCategoryAccess(DatabaseConnection):

    def __init__(self, connection=None, cursor=None):
        super().__init__(connection, cursor)
        self.TABLE_NAME = 'elemental_categorycrossreference'

    #private
    def get_cross_category_id_by_from_to_codes(self, category_code_a, category_code_b):
        '''Will check in booth sides: from and to codes intercalated.'''
        query = ("select id from "
            +self.TABLE_NAME+" where (from_code = "+str(category_code_a)+
            " AND to_code = "+str(category_code_b)+") OR "
            "(from_code = "+str(category_code_b)+" AND to_code = "
            +str(category_code_a)+");" )
        # print(query)
        self.cursor.execute(query)
        categories_ids = self.cursor.fetchall()
        # categories_ids is an array of tuples
        if len(categories_ids)>1:
            raise Exception('Incongruent data in database.')
        elif len(categories_ids)==1:
            # return first array element, with first value in tuple:
            # print("categories ids for cross category: ")
            # pprint(categories_ids)
            return categories_ids[0][0]
        else:
            # print('NO cross category was found.')
            return None

    #private
    def get_cross_category_data_by_id(self, cross_category_id):
        query = ("select id, from_category_id, from_code, to_category_id, to_code, \
                content_cross_reference, explicit_cross_reference, pending from "
                +self.TABLE_NAME+" where id = "+str(cross_category_id)+";")
        self.cursor.execute(query)
        category_data = self.cursor.fetchone()
        return category_data

    def register_cross_category_in_table(self, fields, array_values):
        self.cursor.execute("INSERT INTO "+self.TABLE_NAME+" "+
            fields+" VALUES (%s, %s, %s, %s, %s, %s, %s)", array_values)
        self.connection.commit()


    def update_cross_category_data(self, data_dictionary, id):
        '''
        UPDATE Customers
        SET ContactName = 'Alfred', City= 'Frankfurt'
        WHERE CustomerID = 1;
        '''
        fields_with_values = ''
        for key in data_dictionary:
            fields_with_values += (key+"='"+str(data_dictionary[key])+"', ")
        fields_with_values = fields_with_values[0:-2]

        sql = ("UPDATE "+self.TABLE_NAME+
                " SET "+fields_with_values+
                " WHERE id="+str(id)+";")
        self.cursor.execute(sql)
        self.connection.commit()


    #private
    def get_category_id_by_code(self, code):
        c_access = CategoryAccess(self.connection, self.cursor)
        id = c_access.get_category_id_by_code(code)
        return id

    def get_category_url_by_id(self, id):
        c_access = CategoryAccess(self.connection, self.cursor)
        url = c_access.get_category_url_by_id(id)
        return url