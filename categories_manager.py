#!/usr/bin/python3.6.0
# -*- coding: utf-8 -*-

import io
import re
import pprint
import sys
import time

from os import listdir, unlink
from os.path import isfile, join
from pathlib import Path

bookmarking_folder = str(Path(__file__).resolve().parent.parent)
sys.path.append(bookmarking_folder)
from main_route_file import BM_MODULES_DIR
sys.path.append(BM_MODULES_DIR)

from console_constants import (colors as c, unicode_characters as uc)
from folders_config import SIMPLE_CATEGORIES_FILE, FOLDERS
from category_collection import CategoryCollection
from text_processing import string_array_formatted


def delete_folders_content():
    for key in FOLDERS:
        list( map( unlink, (join( FOLDERS[key],f) for f in listdir(FOLDERS[key])) ) )

# deprecated
def old_texts_categorization(texts_array):
    """
    texts must be an array of texts related to each other, as they will be 
    categorized as a whole.
    """
    ## crear un arreglo con puntuaciones varias para el texto.
    ## segun la puntuacion de pertenencia para las categorias raiz, seleccionar las 3 
    ## mejores candidatas para profundizar la subcategorizacion. 
    ## Registrar estos datos en la base de datos, cuando la llamada sea desde 
    ## categorization (i.e. arg es URL) 
    main_categories = []
    category_connection = CategoryConnection()
    category_connection.execute(load_main_categories, [main_categories], 'get_root_categories')

    texts_categories_scores = []
    texts_category_average = {}
    categories_scores = {}
    for category in main_categories:
        frequencies_hash = load_frequencies(category)

    # for category_file in main_categories_array:
    #     frequencies_hash = load_frequencies(category_file)
        total = 0
        # categories and scores for index
        categories_scores[category] = []
        for index, text in enumerate(texts_array):
            # text score for the category hash that's being analized
            text_score = score(text, frequencies_hash)
            categories_scores[category].append(text_score)
            total += text_score
        texts_category_average[category] = (total)/len(texts_array)

    # print_texts_categories_scores(texts_categories_scores)
    final_category = max(texts_category_average, key=lambda category: texts_category_average[category])
    return final_category

def categorize_text(texts_array):
    category_collection = CategoryCollection()
    return category_collection.texts_categorization(texts_array)

def breakdown_scores_from_main(main_scores):
    category_collection = CategoryCollection()
    return category_collection.breakdown_scores_from_main(main_scores)

def flatten_scores_from_main(main_scores):
    category_collection = CategoryCollection()
    return category_collection.flatten_scores_from_main(main_scores)


def usage_error():
    print(c['red']+"Usage error. Check following examples:"+c['end'])
    print(c['blue']+"Load categories from DEFAULT file, simple_test.txt:"+c['end'])
    print("python categories_manager.py load_categories")
    print(c['blue']+"Load categories from YOUR file:"+c['end'])
    print("python categories_manager.py load_categories my_categories_file.txt")
    print(c['blue']+"OR:"+c['end'])
    print("python categories_manager.py load_categories --file=my_categories_file.txt")
    print(c['blue']+"Categorize ONE text:"+c['end'])
    print("python categories_manager.py categorize_texts \"['this is my text']\"")
    print(c['blue']+"Categorize MULTIPLE texts:"+c['end'])
    print("python categories_manager.py categorize_texts \"['this is my text', 'my second text']\"")
    print(c['blue']+"OR:"+c['end'])
    print("python categories_manager.py categorize_texts --texts=\"['this is my text', 'my second text']\"")


if __name__ == '__main__':
    """ All arguments types are strings. No matter how it is 
    formatted, it will be parsed as a string.
    """
    args_length = len(sys.argv)
    if (args_length > 3) or (args_length == 1):
        print(c['red']+'Wrong nuber of arguments.'+c['end'])
        usage_error()
    else:
        command = sys.argv[1]
        ##############################
        # command to load categories
        ##############################
        if (command == 'load_categories'):
            if (args_length == 3):
                command_args = sys.argv[2]
                if command_args.startswith('--file='):
                    file_name = command_args[7:]
                else:
                    file_name = command_args
            else:
                # there can only be three arguments.
                # provide default categorization file
                file_name = SIMPLE_CATEGORIES_FILE
            # now there's a file name
            # do the hard work.
            print(c['red']+uc['warning']+"!!!This option will delete data, in the database and files."+c['end'])
            answer = input(c['red']+"Are you sure?[Y]es [N]o or [K]eep database content and files at my own risk. "+c['end'])
            
            if answer is 'Y':
                print(c['orange']+uc['warning']+'Deleting database content.'+c['end'])
                time.sleep(3) # Delay 3 secs if user changes mind.
                category_collection = CategoryCollection()
                category_collection.rebuild()
                delete_folders_content()
                category_collection.build_nodes_from(file_name)
            elif(answer is 'K'):
                print(c['orange']+uc['warning']+'Keeping database content.'+c['end'])
                category_collection = CategoryCollection()
                category_collection.build_nodes_from(file_name)
            elif answer is 'N':
                print(c['orange']+'Aborting action. Not loading categories.'+c['end'])
            else:
                print('Option not supported.')


        ##################################
        # command to load categorize text
        ##################################
        elif (command == 'categorize_texts'):
            if (args_length == 2):
                print(c['red']+
                    'Wrong number of arguments for text categorization'
                    +c['end'])
                usage_error()
            else:
                command_args = sys.argv[2]
                if command_args.startswith('--texts='):
                    texts_string = command_args[8:]
                else:
                    texts_string = command_args
                texts_array = string_array_formatted(texts_string)
                if texts_array:
                    # result = texts_categorization(texts_array)
                    # print("Among all, best match is: ")
                    # print(result)
                    category_collection = CategoryCollection()
                    main_scores = category_collection.texts_categorization(texts_array)
                    category_collection.breakdown_scores_from_main(main_scores)
                    category_collection.flatten_scores_from_main(main_scores)

                else:
                    usage_error()

        ##############################
        # command to recalibrate
        ##############################
        elif (command == 'recalibrate'):
            category_collection = CategoryCollection()
            category_collection.recalibrate()
        ##############################
        # anything else: WRONG!
        ##############################
        else:
            print('Wrong command.')
            usage_error()

