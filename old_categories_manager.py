#!/usr/bin/python3.6.0
# -*- coding: utf-8 -*-

import io
import re
import pprint
import sys
import time

from os import listdir, unlink
from os.path import isfile, join
from pathlib import Path

bookmarking_folder = str(Path(__file__).resolve().parent.parent)
sys.path.append(bookmarking_folder+'/bm_traverse_modules/')

from web_access import google_search, user_input_url_to_html, save_url_content
from text_processing import (validate_word,
                            split_by_delimiters,
                            file_input_processing, 
                            extract_text_from_html,
                            string_array_formatted,
                            frequencies_for_cleaned_text)
from category_connection import CategoryConnection
from console_constants import (colors as c, unicode_characters as uc)
from folders_config import FOLDERS, SIMPLE_CATEGORIES_TXT

cross_reference_indication = '(Ver'


def delete_folders_content():
    for key in FOLDERS:
        list( map( unlink, (join( FOLDERS[key],f) for f in listdir(FOLDERS[key])) ) )


def load_categories(arguments, category_connection, simple_function):
    cat_file_name = arguments[0]
    """reads the file that contains the categories in the unesco format, extracts 
    the code to create hierarchized nodes, with the name of the category it looks 
    for html content in wikipedia, cleans it, generates a word weight evaluation, 
    creates files for it and associates it with the node hierarchized data in db.
    """
    nodify_arguments = [category_connection, simple_function]
    file_input_processing(nodify_web_content, cat_file_name, nodify_arguments)

def recalibrate_root_categories():
    return None

def nodify_web_content(finput_line, args):
    """ gets web content for each category defined in a line of the file, then it
    creates a node to be registered in the hierarchy of categories at the database.
    function passed to file input processing.
    """
    def frequencies_for_cleaned_html(file_name):
        segmented_text = extract_text_from_html(file_name, FOLDERS['raw_html_folder'])
        if segmented_text is None: return None

        text_file = open(FOLDERS['text_folder']+file_name+'.txt', "w")
        for word in segmented_text:
            text_file.write(word)
        text_file.close()
        frequencies_file = frequencies_for_cleaned_text(segmented_text, file_name, 
            FOLDERS['clean_text_folder'], FOLDERS['frequencies_folder'])

    category_data = parse_input_line(finput_line)

    # an array with results search
    results = google_search(category_data['name'])

    # asks for user to select url content.
    user_selected_data  = user_input_url_to_html(
    results, category_data['name'], category_data['file_name'], FOLDERS['raw_html_folder']) 
    
    frequencies_for_cleaned_html(category_data['file_name'])
    method_to_call=getattr(args[0],args[1])
    # build_node()
    method_to_call(category_data, user_selected_data)


def parse_cross_codes(words_array):
    categories_codes = []
    for word in words_array:
        if word is 'y':
            continue
        new_word = word
        if word[-1:] == ')' || word[-1:] == ',':
            new_word = word[:-1]
        if word[0] == ',': # this case shouldn't exist.
            new_word = new_word[1:]
        if len(new_word) == 2:
            sibling = categories_codes[len(categories_codes)-1]
            new_word = sibling[:4]+new_word
        if '.' in new_word:
            dot_index = new_word.index('.')
            new_word = new_word[:dot_index]+new_word[dot_index+1:]
        categories_codes.append(new_word)
    return categories_codes

def parse_input_line(line):
    words = line.split()
    string_code = words[0]
    words_category_name = words[1:]
    cross_categories_codes = []
    # explicit cross reference.
    if (cross_reference_indication in words):
        position = words.index(cross_reference_indication)
        category_name = words[1:position]
        cross_categories_codes = parse_cross_codes(words[position+1:])
    category_name = " ".join(words_category_name).lower()
    category_file_name  = category_name.replace(' ', '_')
    category_data = {
        'code': string_code,
        'name': category_name,
        'file_name': category_file_name,
        'cross': cross_categories_codes,
    }
    return category_data

def texts_categorization(texts_array):
    """
    texts must be an array of texts related to each other, as they will be 
    categorized as a whole.
    """
    ## crear un arreglo con puntuaciones varias para el texto.
    ## segun la puntuacion de pertenencia para las categorias raiz, seleccionar las 3 
    ## mejores candidatas para profundizar la subcategorizacion. 
    ## Registrar estos datos en la base de datos, cuando la llamada sea desde 
    ## categorization (i.e. arg es URL) 
    main_categories = []
    category_connection = CategoryConnection()
    category_connection.execute(load_main_categories, [main_categories], 'get_root_categories')

    texts_categories_scores = []
    texts_category_average = {}
    categories_scores = {}
    for category in main_categories:
        frequencies_hash = load_frequencies(category)

    # for category_file in main_categories_array:
    #     frequencies_hash = load_frequencies(category_file)
        total = 0
        # categories and scores for index
        categories_scores[category] = []
        for index, text in enumerate(texts_array):
            # text score for the category hash that's being analized
            text_score = score(text, frequencies_hash)
            categories_scores[category].append(text_score)
            total += text_score
        texts_category_average[category] = (total)/len(texts_array)

    # print_texts_categories_scores(texts_categories_scores)
    final_category = max(texts_category_average, key=lambda category: texts_category_average[category])
    return final_category


# deprecated.
# def print_texts_categories_scores(texts_categories_scores):
#     for index, categories in enumerate(texts_categories_scores):
#         print('Text with index: '+index)
#         for category in categories:
#             print('  Category: '+category)
#             print('  Score: '+texts_categories_scores[index][category])


def load_main_categories(arguments, category_connection, simple_function):
    main_categories = arguments[0]
    method_to_call=getattr(category_connection, simple_function)
    categories = method_to_call()
    for category in categories:
        #file_name = FOLDERS['frequencies_folder']+(category[4].replace(' ', '_'))+'.frq'
        main_categories.append(category[4])
    


# deprecated 
def get_main_categories_files():
    only_files = [f for f in listdir(FOLDERS['frequencies_folder']) if isfile(join(FOLDERS['frequencies_folder'], f))]
    return only_files


def load_frequencies(category_name):
    file_route = FOLDERS['frequencies_folder']+(category_name.replace(' ', '_'))+'.frq'
    frequencies = {}
    print('file_route: '+file_route)
    with open(file_route, 'r', encoding='utf-8') as file:
        line = file.readline()
        while line:
            # print('line: '+line+'.')
            match = re.match(r"\"(?P<word>\w+)\":(?P<frec>[+-]?([0-9]*[.])?[0-9]+)", line)
            # print(match['word'])
            # print(match['frec'])
            frequencies[match['word']] = float(match['frec'])
            line = file.readline()
    return frequencies


def score(text, category_frequencies):
    score = 0.0
    shorter_words = split_by_delimiters(text)
    for shorter_word in shorter_words:
        word_to_inspect = shorter_word.lower()
        # print(word_to_inspect)
        if word_to_inspect in category_frequencies:
            print('word: '+word_to_inspect+' frequency: '+str(category_frequencies[word_to_inspect]))
            score += category_frequencies[word_to_inspect]
    print('final score: %.6f' %score)
    return (1000.0*score/len(shorter_words))

def usage_error():
    print(c['red']+"Usage error. Check following examples:"+c['end'])
    print(c['blue']+"Load categories from DEFAULT file, simple_test.txt:"+c['end'])
    print("python categories_manager.py load_categories")
    print(c['blue']+"Load categories from YOUR file:"+c['end'])
    print("python categories_manager.py load_categories my_categories_file.txt")
    print(c['blue']+"OR:"+c['end'])
    print("python categories_manager.py load_categories --file=my_categories_file.txt")
    print(c['blue']+"Categorize ONE text:"+c['end'])
    print("python categories_manager.py categorize_texts \"['this is my text']\"")
    print(c['blue']+"Categorize MULTIPLE texts:"+c['end'])
    print("python categories_manager.py categorize_texts \"['this is my text', 'my second text']\"")
    print(c['blue']+"OR:"+c['end'])
    print("python categories_manager.py categorize_texts --texts=\"['this is my text', 'my second text']\"")


if __name__ == '__main__':
    """ All arguments types are strings. No matter how it is 
    formatted, it will be parsed as a string.

    """
    args_length = len(sys.argv)
    ## file_name = sys.argv[1]
    ## load_categories(file_name)
    if (args_length > 3) or (args_length == 1):
        print(c['red']+'Wrong nuber of arguments.'+c['end'])
        usage_error()
    else:
        command = sys.argv[1]
        if (command == 'load_categories'):
            if (args_length == 3):
                command_args = sys.argv[2]
                if command_args.startswith('--file='):
                    file_name = command_args[7:]
                else:
                    file_name = command_args
            else:
                # it can only be three arguments.
                # provide default categorization file
                file_name = SIMPLE_CATEGORIES_TXT
            # now there's a file name
            # do the hard work.
            print(c['red']+uc['warning']+"!!!This option will delete data in database."+c['end'])
            answer = input(c['red']+"Are you sure?[Y]es [N]o"+c['end'])
            if answer is 'Y':
                print(c['orange']+'Deleting database content.'+c['end'])
                time.sleep(3) # Delay 3 secs if user changes mind.
                category_connection = CategoryConnection()
                category_connection.rebuild()
                delete_folders_content()
                category_connection.execute(load_categories, [file_name], 'build_node')
            elif answer is 'N':
                print('Aborting.')
            else:
                print('Option not supported.')

        elif (command == 'categorize_texts'):
            if (args_length == 2):
                print(c['red']+
                    'Wrong number of arguments for text categorization'
                    +c['end'])
                usage_error()
            else:
                command_args = sys.argv[2]
                if command_args.startswith('--texts='):
                    texts_string = command_args[8:]
                else:
                    texts_string = command_args
                texts_array = string_array_formatted(texts_string)
                if texts_array:
                    result = texts_categorization(texts_array)
                    print("Among all, best match is: ")
                    print(result)
                else:
                    usage_error()
        else:
            print('Wrong command.')
            usage_error()

