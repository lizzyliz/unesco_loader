#!/usr/bin/python3.6.0
# -*- coding: utf-8 -*-

from database_connection import DatabaseConnection
from web_user_access import WebUserAccess
from folders_config import FOLDERS

# abstract
class CategoryAccess(DatabaseConnection):
    def __init__(self, connection=None, cursor=None):
        super().__init__(connection, cursor)
        self.web_user_access = WebUserAccess()
        self.TABLE_NAME = 'elemental_category'

        self.RAW_HTML_FOLDER = FOLDERS['raw_html_folder']
        self.TEXT_FOLDER = FOLDERS['text_folder']
        self.CLEAN_TEXT_FOLDER = FOLDERS['clean_text_folder']
        self.FREQUENCIES_FOLDER = FOLDERS['frequencies_folder']

    # private 
    def get_categories_ids_with_url_except(self, url, id):
        query = ("select id from "
            "elemental_category where url_content = '"+url+"' "
            "except select id from elemental_category where id="+str(id)+";")
        self.cursor.execute(query)
        categories_ids = self.cursor.fetchall()
        if categories_ids is None: categories_ids = []
        return categories_ids

    # private 
    def get_categories_codes_with_url_except(self, url, code):
        query = ("select code from "
            "elemental_category where url_content = '"+url+"' "
            "except select code from elemental_category where code="+str(code)+";")
        self.cursor.execute(query)
        categories_codes = self.cursor.fetchall()
        if categories_codes is None: categories_codes = []
        return categories_codes


    #private
    def get_category_id_by_code(self, code):
        self.cursor.execute("SELECT id FROM "+self.TABLE_NAME+
            " WHERE code="+str(code)+";")
        category_data = self.cursor.fetchone()
        if category_data is None: return None
        return category_data[0]

    def get_category_url_by_id(self, id):
        self.cursor.execute("SELECT url_content FROM "+self.TABLE_NAME+
            " WHERE id="+str(id)+";")
        category_data = self.cursor.fetchone()
        if category_data is None: return None
        return category_data[0]

    #private
    def get_category_data_by_id(self, category_id):
        query = ("select id, code, root_code, trail_code, name, "
                "url_content, title_content, description_content, "
                "total_words, parent_category_id from "
                "elemental_category where id = "+str(category_id)+";")
        self.cursor.execute(query)
        category_data = self.cursor.fetchone()
        return category_data

    #private
    def get_children_ids_for(self, root_id = None):
        category_id = '= '+str(root_id)
        if root_id is None: category_id = 'is null'
        query = "select id from elemental_category where parent_category_id %s;" %category_id
        self.cursor.execute(query)
        categories_ids = self.cursor.fetchall()
        return categories_ids

    # private
    def register_category_in_table(self, fields, values):
        self.cursor.execute("INSERT INTO "+self.TABLE_NAME+" "+
            fields+" VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)", values)
        self.connection.commit()
    
    def update_category_total_words(self, total_words, id):
        sql = "UPDATE elemental_category SET total_words = "+str(total_words)+" WHERE id = "+str(id)+";"
        self.cursor.execute(sql)
        self.connection.commit()
