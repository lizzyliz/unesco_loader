#!/usr/bin/python3.6.0
# -*- coding: utf-8 -*-

import psycopg2
import subprocess
import sys
from pprint import pprint

from main_route_file import (BM_MODULES_DIR, BOOKMARKING_APP)
from databases_config import ACTUAL_DATABASE as database_config

class DatabaseConnection():
    def __init__(self, connection=None, cursor=None):
        self.database   = database_config['default']
        self.connection = connection
        self.cursor     = cursor

    def connect(self):
        try:
            self.connection = psycopg2.connect(
                host        = self.database['HOST'], 
                user        = self.database['USER'], 
                password    = self.database['PASSWORD'], 
                dbname      = self.database['NAME']
            )
            self.cursor = self.connection.cursor()
        except:
            print('Connection not established.')

    def close(self):
        self.connection.close()

    def rebuild(self):
        '''Should rebuild just categories related tables BUT 
        since that is inconsistent, it rebuilds whole database.'''
        # self.close_connections()
        from databases_config import ACTUAL_PHASE as phase
        try:
            database_script = BM_MODULES_DIR+'database_create.sh'
            django_manager = BOOKMARKING_APP+'/manage.py'
            settings    =  "--settings=config.settings.%s"%(phase)
            db_name     = self.database['NAME']
            db_user     = self.database['USER']
            db_password = self.database['PASSWORD']
            print('-------------------------------')
            print('REBUILDING DATABASE: '+db_name)
            print('-------------------------------')
            subprocess.call(['bash', database_script, db_name, db_user, db_password])
            subprocess.call(['python', django_manager, 'makemigrations', settings])
            subprocess.call(['python', django_manager, 'migrate', settings])
        except Exception as e:
            print(e)
            print("Couldn't rebuild database. Manual execution needed.")

    # private
    # shouldn't use this method. Same session cannot be closed.
    def close_connections(self):
        session_end_query = (
            "SELECT pg_terminate_backend(pg_stat_activity.pid) "
            "FROM pg_stat_activity "
            "WHERE pg_stat_activity.datname = '{db_name}' "
            "AND pid <> pg_backend_pid();"
        ).format(db_name=self.database['NAME'])
        try:
            self.connect()
            self.cursor.execute(session_end_query)
            self.connection.commit()
        except Exception as e:
            print(e)
            print("Can't close connections.")
    

    #private
    def dictionary_steamroller(self, dict_tuple, string_scape = False):
        # print(type(dict_tuple))
        s = '('
        # pprint(dict_tuple)
        for element in dict_tuple:
            if string_scape and isinstance(element, str):
                s+="'"+element+"'"+', '
            elif element is None: 
                s+='NULL, '
            else:
                s+=element+', '
        final_s = s[:-2]+')'
        return final_s