#!/usr/bin/python3.6.0
# -*- coding: utf-8 -*-

import sys
import math 

from text_processing import (file_input_processing,
                            split_by_delimiters,
                            clean_and_segment_texts)
from category_access import CategoryAccess
from category import Category
from category_score import CategoryScore
from pprint import pprint
from console_constants import colors as c

from main_route_file import BM_MODULES_DIR
sys.path.append(BM_MODULES_DIR)

from bm_traverse_modules.web_constants import (MAX_MAIN_CATEGORIZATION_DEPTH,
                            MAX_MIDDLE_CATEGORIZATION_DEPTH, MAX_ALL_CATEGORIZATION,
                            DEEPEST_LEVEL, CATEGORIZATION_METHOD)

cross_reference_indication = '(Ver'

class CategoryCollection(CategoryAccess):
    
    def __init__(self):
        self.categories = []
        super().__init__()


    def recalibrate(self):
        self.connect()
        children_ids = self.get_children_ids_for(None)
        for child_id in children_ids:
            category_data = {'id': child_id[0]}
            category = Category(category_data, self.connection, self.cursor)
            category.recalibrate()
        self.close()

    ######
    # checked
    # private 
    def parse_cross_codes(self, words_array):
        # pprint(words_array)
        categories_codes = []
        for word in words_array:
            # print(word)
            if word is 'y':
                continue
            new_word = word
            if word[-1:] == ')' or word[-1:] == ',':
                new_word = word[:-1]
            # print('new_word: '+new_word)
            if word[0] == ',': # this case shouldn't exist.
                new_word = new_word[1:]
            if len(new_word) == 2:
                sibling = str(categories_codes[len(categories_codes)-1])
                new_word = sibling[:4]+new_word
            if '.' in new_word:
                dot_index = new_word.index('.')
                new_word = new_word[:dot_index]+new_word[dot_index+1:]
            categories_codes.append(int(new_word))
        return categories_codes

    # checked
    # private
    def line_to_category_data(self, line):
        words = line.split()
        string_code = words[0]
        category_name = words[1:]
        cross_categories_codes = []
        # explicit cross reference.
        if (cross_reference_indication in words):
            position = words.index(cross_reference_indication)
            category_name = words[1:position]
            cross_categories_codes = self.parse_cross_codes(words[position+1:])
            
        category_name = " ".join(category_name).lower()

        category_file_name  = category_name.replace(' ', '_')
        category_data = {
            'code': string_code,
            'name': category_name,
            'file_name': category_file_name,
            'cross': cross_categories_codes,
        }
        return category_data
 
    # checked
    #private
    def create_node(self, line):
        """ gets web content for each category defined in a line of the file, then 
        it creates a node to be registered in the hierarchy of categories at the 
        database. function passed to file input processing.
        """
        category_data = self.line_to_category_data(line)
        category = Category(category_data, self.connection, self.cursor)
        # TODO: llamar a recalibrar, y a volver a verificar los cross_categories.
        self.categories.append(category)

    # checked
    # public
    def build_nodes_from(self, file_name):
        """reads the file that contains the categories in the unesco format, extracts 
        the code to create hierarchized nodes, with the name of the category it looks 
        for html content in wikipedia, cleans it, generates a word weight evaluation, 
        creates files for it and associates it with the node hierarchized data in db.
        This method doesn't validate if input text with codes and categories is consistent.
        It assumes that the file codes are correctly hierarchized.
        """
        self.connect()
        file_input_processing(self.create_node, file_name)
        self.close()
        self.done_message_admin()


    #private - turning into public because I need this method in django's categories view.
    def load_sub_categories_for(self, category_id = None):
        sub_categories = []
        children_ids = self.get_children_ids_for(category_id)
        for child_id in children_ids:
            category_data = {'id': child_id[0]}
            category = Category(category_data, self.connection, self.cursor)
            sub_categories.append(category)
        return sub_categories


    # private 
    def shrink_into_scores(self, code_score_tuple, limit):
        categories_scores = [] # array of scores.
        number_categories_scores = 0
        print('SHRINKING')
        for code, score in code_score_tuple:
            print('code: '+str(code))
            print('score: '+str(score))
            if (score == 0.0) or (number_categories_scores == limit):
                return categories_scores
            else:
                category = self.get_category_by_code(code)
                score_object = CategoryScore(score, category)
                categories_scores.append(score_object)
                number_categories_scores+=1
        return categories_scores


    def generate_scores_for(self, segmented_words, categories_scores):
        if len(categories_scores) > 0:
            for score_object in categories_scores:
                category = score_object.category
                sub_categories = self.load_sub_categories_for(category.id)
                # sub_scores = []
                if len(sub_categories) > 0:
                    code_score_tuple = self.score_with_categories(segmented_words, sub_categories)
                    shrinked_scores = self.shrink_into_scores(code_score_tuple, MAX_MIDDLE_CATEGORIZATION_DEPTH)
                    score_object.add_sub_category_scores(shrinked_scores) # in class.
                    self.generate_scores_for(segmented_words, shrinked_scores)
                else:
                    # do nothing?
                    None
        else:
            # return what has been done with the categories_scores
            # or None, this fits better.
            return categories_scores
            return None
        


    # public 
    def texts_categorization(self, texts_array):
        if len(texts_array) == 0: return []

        segmented_words = clean_and_segment_texts(texts_array)
        self.connect()
        # firstly, main categories.
        categories = self.load_sub_categories_for()
        shrinked_scores = []
        if len(categories) > 0:
            # score with categories returns a sorted key value tuple.
            code_score_tuple = self.score_with_categories(segmented_words, categories)
            shrinked_scores = self.shrink_into_scores(code_score_tuple, MAX_MAIN_CATEGORIZATION_DEPTH)
            if len(shrinked_scores) > 0:
                # score_object.add_sub_scores(shrinked_scores) # in class.
                self.generate_scores_for(segmented_words, shrinked_scores)
        # shrinked_scores has the results.
        self.close()
        print('---------------------------------')
        print('---    DONE SCORES RESULT     ---')
        print('---------------------------------')
        #######
        # Hasta aqui es imprimir en consola.
        if len(shrinked_scores) > 0:
            for category_score in shrinked_scores:
                category_score.print_content()

        '''
        reshrinked_scores = []
        actual_level = DEEPEST_LEVEL
        reshrinked_scores = self.reshrink_scores(shrinked_scores, reshrinked_scores, actual_level)

        print('---------------------------')
        print('---  DONE SCORES RESULT ---')
        print('---------------------------')
        if len(reshrinked_scores) > 0:
            for category_score in reshrinked_scores:
                category_score.print_content()
        '''
        return shrinked_scores


    # private deprecated 
    def sort_reverse_scores(self, shrinked_scores):
        if len(shrinked_scores) == 0:
            return shrinked_scores
        else:
            reverse_order = [[]]
            root_score_nodes = shrinked_scores
            for root in root_score_nodes:
                reverse_order, max_index = self.save_in_reverse_order(reverse_order, root, 0)

        self.print_reverse_order(reverse_order)


    # private deprecated 
    def print_reverse_order(self, reverse_order):
        for level_elements in reverse_order:
            print('NIVEL')
            pprint(level_elements)
            for element in level_elements:
                element.print_content()


    # private deprecated 
    def save_in_reverse_order(self, reverse_order, root_category_score, index):
        root_children = root_category_score.sub_category_scores

        if len(root_children) != 0:
            for child in root_children:
                reverse_order, index = self.save_in_reverse_order(reverse_order, child, index)
                # index = index+1

            # pprint(reverse_order)
            index = index+1

            if len(reverse_order) < index:
                reverse_order.append([])

        else:
            reverse_order[index].append(root_category_score)
            # index = index -1 

        return reverse_order, index


    # private, deprecated
    def get_max_level_scores(self, level_scores):
        # MAX_ALL_CATEGORIZATION
        if len(level_scores) == 0:
            return []
        else: 
            # sort scores.
            level_scores.sort(key=lambda x: x.score, reverse=True)
            return level_scores[0:MAX_ALL_CATEGORIZATION]


    # private, deprecated
    def reshrink_scores(self, shrinked_scores, max_level_scores, actual_level):
        if len(shrinked_scores) == 0:
            return shrinked_scores

        all_level_scores = []
        for category_score in shrinked_scores:
            category_level_scores = category_score.get_level_scores(actual_level)
            category_max_level_scores = self.get_max_level_scores(category_level_scores)
            all_level_scores.extend(category_max_level_scores)

        max_level_scores = self.get_max_level_scores(all_level_scores)

        if len(max_level_scores)  >= MAX_ALL_CATEGORIZATION:
            return max_level_scores
        else:
            new_level = actual_level-1
            if (new_level>=0):
                return self.reshrink_scores(shrinked_scores, max_level_scores, new_level)
            else:
                return max_level_scores


    # public 
    def flatten_scores_from_main(self, shrinked_scores):
        print('----------------------')
        print('--- FLATTEN RESULT ---')
        print('----------------------')
        flatten_scores = []
        if len(shrinked_scores) > 0:
            for category_score in shrinked_scores:
                category_scores_array = category_score.flatten()
                flatten_scores += category_scores_array 
        # pprint(flatten_scores)
        for score in flatten_scores:
            print(score)
        return flatten_scores

    # public 
    def breakdown_scores_from_main(self, shrinked_scores):
        #######
        # Alistando resultado parseable para la app web
        print('-------------------------')
        print('--- BREAK DOWN RESULT ---')
        print('-------------------------')
        final_category_scores = []

        if len(shrinked_scores) > 0:
            for category_score in shrinked_scores:
                appended_categories = category_score.get_appended_sub_scores()
                for appended_category in appended_categories:
                    final_category_scores.append(appended_category)
            pprint(final_category_scores)
            
        return final_category_scores


    
    #private
    def score_with_categories(self, segmented_words, categories):
        # main_categories = self.load_sub_categories_for()
        text_categories_scores = {}
        categories_total_words = 0.0 # total sum of words in this level.

        # primera versión de level_total_words
        # level_total_words = 0
        # for category in categories: level_total_words += category.total_words
        # print('mostrando level_total_words: '+str(level_total_words))

        # segunda versión de level_total_words
        z = {}
        for category in categories: 
            frequencies_hash = category.load_frequencies()
            z = {**z, **frequencies_hash} # merges two dictionaries: z and frequencies_hash
        level_total_words = len(z)
        print('mostrando level_total_words: '+str(level_total_words))


        for category in categories:
            print('*** nombre de categoría: '+category.name+' ***')
            frequencies_hash = category.load_frequencies()
            total_words     = category.total_words
            # print('this is the text: '+' '.join(segmented_words))
            # COMMENT: OLD calculation
            if CATEGORIZATION_METHOD == 'lineal':
                category_score  = self.score(segmented_words, frequencies_hash, total_words)

            if CATEGORIZATION_METHOD == 'bayesian':
                category_score  = self.bayesian_score(segmented_words, frequencies_hash, total_words)
                categories_total_words += category_score

            # por defecto, es log_bayesian.
            else:
                category_score  = self.log_bayesian_score(segmented_words, frequencies_hash, total_words, level_total_words)
                
            print(category_score)
            text_categories_scores[category.code] = category_score


        if CATEGORIZATION_METHOD == 'bayesian':
            print('cantidad total de palabras consideradas en el nivel: '+(str(categories_total_words)) )
            if categories_total_words > 0.0:
                for category_code in text_categories_scores:
                    text_categories_scores[category_code] = (text_categories_scores[category_code]/categories_total_words)*100

        # print_texts_categories_scores(texts_categories_scores)
        # sort categories by score. Max first.
        key_value_tuple = [(k, text_categories_scores[k]) 
            for k in sorted(text_categories_scores,
                key=text_categories_scores.get, reverse=True)]

        self.print_scores(key_value_tuple)
        return key_value_tuple


    def log_bayesian_score(self, segmented_words, frequencies_hash, total_words, level_total_words):
        from math import log
        text_score = 0.0
        total_words = total_words*1.0

        if total_words == 0.0:
            print('-- Category has no words --')
            return 0.0

        for word in segmented_words:
            # print(word_to_inspect)
            if word in frequencies_hash:
                print('word: '+word+
                   ' frequency: '+str(frequencies_hash[word]))
                value = frequencies_hash[word]
            else:
                value = 0
            numerator = (value+1)
            denominator = total_words+level_total_words

            text_score += log(numerator/denominator)

        final_category_score = log(total_words/level_total_words)+text_score

        print('sum text_score: %.6f'%final_category_score)
        '''
        if (final_category_score > 0.0):
            final_score = 1000.0*(final_category_score/len(segmented_words))
        else:
            final_score = 0.0
        '''
        ### if(final_category_score <0.0):
        ###     final_category_score*= -1
        # print('final score: '+str(final_score))
        return final_category_score


    # private
    def bayesian_score(self, segmented_words, frequencies_hash, total_words):
        text_score = 0.0
        total_words = total_words*1.0

        if total_words == 0.0:
            print('-- Category has no words --')
            return 0.0

        for word in segmented_words:
            # print(word_to_inspect)
            if word in frequencies_hash:
                print('word: '+word+
                   ' frequency: '+str(frequencies_hash[word]))
                text_score += (frequencies_hash[word])

        return text_score


    # private
    def score(self, segmented_words, frequencies_hash, total_words):
        text_score = 0.0
        total_words = total_words*1.0

        if total_words == 0.0:
            print('-- Category has no words --')
            return 0.0

        for word in segmented_words:
            # print(word_to_inspect)
            if word in frequencies_hash:
                print('word: '+word+
                   ' frequency: '+str(frequencies_hash[word]))
                text_score += (frequencies_hash[word])

        final_category_score = (text_score/total_words)
        print('sum text_score: %.6f'%final_category_score)
        if (final_category_score > 0.0):
            final_score = 1000.0*(final_category_score/len(segmented_words))
        else:
            final_score = 0.0
        # print('final score: '+str(final_score))
        return final_score


    def print_scores(self, k_v_tuple):
        print('---------------------------')
        print(' printing scores for text  ')
        print('---------------------------')
        for k, v in k_v_tuple:
            category = self.get_category_by_code(k)
            print('score: '+str(v))
            print('code: '+str(category.code)+'  name: '+category.name)
            print()


    # returns one category.
    def get_category_by_code(self, code):
        category_id = self.get_category_id_by_code(code)
        category_data = { 'id': category_id }
        category = Category(category_data, self.connection, self.cursor)
        return category


    def done_message_admin(self):
        print("DONE.")
        print("Congratulations, you've finished!")
        print("                                   .''.")
        print("       .''.      .        *''*    :_\/_:     .")
        print("      :_\/_:   _\(/_  .:.*_\/_*   : /\ :  .'.:.'.")
        print("  .''.: /\ :    /)\   ':'* /\ *  : '..'.  -=:o:=-")
        print(" :_\/_:'.:::.  | ' *''*    * '.\'/.'_\(/_'.':'.'")
        print(" : /\ : :::::  =  *_\/_*     -= o =- /)\    '  *")
        print("  '..'  ':::' === * /\ *     .'/.\'.  ' ._____")
        print('      *        |   *..*         :       |.   |\' .---"|')
        print("        *      |     _           .--'|  ||   | _|    |")
        print("        *      |  .-'|       __  |   |  |    ||      |")
        print("     .-----.   |  |' |  ||  |  | |   |  |    ||      |")
        print(" ___'       ' /\"\ |  '-.\"\".    '-'   '-.'    '`      |____")
        print(c['blue']+"jgs~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"+c['end'])
        print("  &                    ~-~-~-~-~-~-~-~-~-~   /|")
        print(" ejm97    )      ~-~-~-~-~-~-~-~  /|~       /_|\\")
        print("        _-H-__  -~-~-~-~-~-~     /_|\    -~======-~")
        print("~-\XXXXXXXXXX/~     ~-~-~-~     /__|_\ ~-~-~-~")
        print("~-~-~-~-~-~    ~-~~-~-~-~-~    ========  ~-~-~-~")
        