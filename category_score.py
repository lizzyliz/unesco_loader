#!/usr/bin/python3.6.0
# -*- coding: utf-8 -*-

from pprint import pprint

class CategoryScore():

	# public
	def __init__(self, score, category):
		self.score = score
		self.category = category
		self.sub_category_scores = []

	# public
	def add_sub_category_scores(self, sub_category_scores):
		self.sub_category_scores = sub_category_scores

	def flatten(self):
		array = []
		self_hash = {
			'code': self.category.code,			
			'name': self.category.name,
			'degree': self.score			
		}
		array.append(self_hash)
		for sub_category_score in self.sub_category_scores:
			sub_array = sub_category_score.flatten()
			array += sub_array
		return array

	# private
	def calculate_level(self):
		self_code = str(self.category.code)
		if len(self_code) == 4:
			if self_code[2:4] == '00':
				return 0 # level is zero
			else:
				return 1 # level is one

		elif len(self_code) == 6:
			return 2  # level is two

		elif len(self_code) == 7:
			return 3  # level is three

		else:
			return None		

	# should only BE for parents. But not.
	# if applied to a category score that has no higher levels, should return None.

	def get_level_scores(self, level):
		self_level = self.calculate_level()

		if self_level == level:
			return [self]
		else:
			level_scores = []

			if (self_level<level):
				for sub_category_scores in self.sub_category_scores:
					scores = sub_category_scores.get_level_scores(level)
					level_scores.extend(scores)

		return level_scores

	def print_content(self):
		print('===============================================')
		print('score: '+str(self.score))
		print('category name: '+self.category.name)
		print('category code: '+str(self.category.code))
		print('-----------------------------------------------')
		for sub_category_score in self.sub_category_scores:
			sub_category_score.print_content()

	# NOT working fine, TODO: tune it up.
	def get_appended_sub_scores(self):
		if len(self.sub_category_scores)>0:
			new_appended_sub_scores = []  

			for sub_category_score in self.sub_category_scores:
				# print(self.category.name)
				appended_sub_scores = sub_category_score.get_appended_sub_scores()
				self_hash = {
					'code': self.category.code,			
					'name': self.category.name,
					'degree': self.score			
				}
				# print(len(appended_sub_scores))
				
				for categories_hash_array in appended_sub_scores:
					categories_hash_array.insert(0, self_hash)

				new_appended_sub_scores = new_appended_sub_scores+appended_sub_scores
				
			return new_appended_sub_scores

		else:
			self_hash = {
				'code': self.category.code,			
				'name': self.category.name,
				'degree': self.score

			}
			return [[self_hash]]

	def __str__(self):
		return ('category code: '+str(self.category.code)+' category name: '+self.category.name)
		
