#!/usr/bin/python3.6.0
# -*- coding: utf-8 -*-
# python test_load_categories.py TestLoadCategories.test_root_with_children
import os
import sys
import unittest
import psycopg2
import json
import time

from pathlib import Path
from pprint import pprint

BOOKMARKING_FOLDER = str(Path(__file__).resolve().parent.parent.parent)
sys.path.append(BOOKMARKING_FOLDER)
from main_route_file import BM_MODULES_DIR, UNESCO_DIR
sys.path.append(BM_MODULES_DIR)
sys.path.append(UNESCO_DIR)

from  categories_manager import delete_folders_content
from  database_connection import DatabaseConnection
from category_collection import CategoryCollection



from folders_config import FOLDERS, TEST_FOLDER_CATEGORIES

class TestCrossReferences(unittest.TestCase):
    """ 
    Tests are made directly by calling modules functions or objects, 
    NOT by console interaction.
    Before every load_categories execution, database should be rebuild 
    and folders with generated files should be emptied.
    """
    def __test_database_rebuild(self, category_collection):
        print('----->  Inner testing: Test database rebuild. ---')
        # setting environment for test.
        tables_so_far = [
            'auth_group', 
            'auth_group_permissions', 
            'auth_permission', 
            'auth_user', 
            'auth_user_groups', 
            'auth_user_user_permissions', # 6 tables for auth 
            'django_admin_log', 
            'django_content_type', 
            'django_migrations', 
            'django_session', # 4 tables for django
            'elemental_category', 
            'elemental_categorycrossreference', 
            'elemental_reference', 
            'elemental_webpage', 
            'elemental_webpagecategory', # 5 tables for elemental
            'social_auth_association', 
            'social_auth_code', 
            'social_auth_nonce', 
            'social_auth_partial', 
            'social_auth_usersocialauth', # 5 tables for social auth
            'star_ratings_rating', 
            'star_ratings_userrating', # 2 tables for ratings
        ]
        query_database_tables = ( "select tablename from pg_catalog.pg_tables "
                                "WHERE schemaname != 'pg_catalog' "
                                "AND schemaname != 'information_schema' "
                                "ORDER BY tablename;")
        tables_to_ommit = [
            'auth_permission',
            'django_content_type',
            'django_migrations'
        ]
        category_collection.rebuild()
        category_collection.connect()
        # Then validate results in two steps.
        category_collection.cursor.execute(query_database_tables)
        results = category_collection.cursor.fetchall()
        # first validate same amount of tables.
        self.assertEqual(len(results), len(tables_so_far))
        # second, compare each table element existance.
        # third, validate that each table is empty.
        base_count_query = "SELECT COUNT(*) FROM "
        for i, result in enumerate(results):
            self.assertEqual(tables_so_far[i], result[0])
            print('Table: '+tables_so_far[i])
            if not(tables_so_far[i] in tables_to_ommit):
                query = base_count_query+tables_so_far[i]+";"
                category_collection.cursor.execute(query)
                tuples_in_table = category_collection.cursor.fetchone()
                self.assertEqual(tuples_in_table[0], 0)
        category_collection.close()


    def __test_emptied_folders(self):
        print('----->  Inner testing: Test emptied folders. ---')
        delete_folders_content()
        for key, folder in FOLDERS.items():
            self.assertTrue((os.listdir(folder) == []))


    def test_content_cross_reference(self):
        print('--------------------------------------------')
        print('    MAIN TEST: Test content cross reference ')
        print('--------------------------------------------')
        file_name_in = TEST_FOLDER_CATEGORIES+"cr_1_content_cross_reference.in"
        file_name_out = TEST_FOLDER_CATEGORIES+"cr_1_content_cross_reference.out"

        category_collection = CategoryCollection()
        self.__test_database_rebuild(category_collection)
        self.__test_emptied_folders()
        category_collection.build_nodes_from(file_name_in)

        fetch_query = ("select from_code, to_code, content_cross_reference, "
                        "explicit_cross_reference, pending "
                        "from elemental_categorycrossreference order by id;")
      
        category_collection.connect()
        category_collection.cursor.execute(fetch_query)
        results = category_collection.cursor.fetchall()

        with open(file_name_out) as f:
            data = json.load(f)
            # validate same amount of query rows.
            print(data)
            self.assertEqual(len(results), len(data))
            # second, compare each element
            for i, result in enumerate(results):
                self.assertEqual(result, tuple(data[i].values()))
        category_collection.close()


    def test_explicit_cross_reference_one(self):
        print('-------------------------------------------------')
        print('    MAIN TEST: Test explicit ONE cross reference ')
        print('-------------------------------------------------')
        file_name_in = TEST_FOLDER_CATEGORIES+"cr_2_explicit_cross_reference_one.in"
        file_name_out = TEST_FOLDER_CATEGORIES+"cr_2_explicit_cross_reference_one.out"

        category_collection = CategoryCollection()
        self.__test_database_rebuild(category_collection)
        self.__test_emptied_folders()
        category_collection.build_nodes_from(file_name_in)

        fetch_query = ("select from_code, to_code, "
                        "explicit_cross_reference, pending "
                        "from elemental_categorycrossreference order by id;")
      
        category_collection.connect()
        category_collection.cursor.execute(fetch_query)
        results = category_collection.cursor.fetchall()

        with open(file_name_out) as f:
            data = json.load(f)
            # validate same amount of query rows.
            print(data)
            self.assertEqual(len(results), len(data))
            # second, compare each element
            for i, result in enumerate(results):
                self.assertEqual(result, tuple(data[i].values()))
        category_collection.close()


    def test_explicit_cross_reference_two(self):
        print('-------------------------------------------------')
        print('    MAIN TEST: Test explicit TWO cross reference ')
        print('-------------------------------------------------')
        file_name_in = TEST_FOLDER_CATEGORIES+"cr_2_explicit_cross_reference_two.in"
        file_name_out = TEST_FOLDER_CATEGORIES+"cr_2_explicit_cross_reference_two.out"

        category_collection = CategoryCollection()
        self.__test_database_rebuild(category_collection)
        self.__test_emptied_folders()
        category_collection.build_nodes_from(file_name_in)

        fetch_query = ("select from_code, to_code, "
                        "explicit_cross_reference, pending "
                        "from elemental_categorycrossreference order by id;")
      
        category_collection.connect()
        category_collection.cursor.execute(fetch_query)
        results = category_collection.cursor.fetchall()

        with open(file_name_out) as f:
            data = json.load(f)
            # validate same amount of query rows.
            print(data)
            self.assertEqual(len(results), len(data))
            # second, compare each element
            for i, result in enumerate(results):
                self.assertEqual(result, tuple(data[i].values()))
        category_collection.close()


    def test_explicit_cross_reference_three_1(self):
        print('-----------------------------------------------------')
        print('    MAIN TEST: Test explicit THREE cross reference 1 ')
        print('-----------------------------------------------------')
        file_name_in = TEST_FOLDER_CATEGORIES+"cr_2_explicit_cross_reference_three_1.in"
        file_name_out = TEST_FOLDER_CATEGORIES+"cr_2_explicit_cross_reference_three_1.out"

        category_collection = CategoryCollection()
        self.__test_database_rebuild(category_collection)
        self.__test_emptied_folders()
        category_collection.build_nodes_from(file_name_in)

        fetch_query = ("select from_code, to_code, "
                        "explicit_cross_reference, pending "
                        "from elemental_categorycrossreference order by id;")
      
        category_collection.connect()
        category_collection.cursor.execute(fetch_query)
        results = category_collection.cursor.fetchall()

        with open(file_name_out) as f:
            data = json.load(f)
            # validate same amount of query rows.
            print(data)
            self.assertEqual(len(results), len(data))
            # second, compare each element
            for i, result in enumerate(results):
                self.assertEqual(result, tuple(data[i].values()))
        category_collection.close()


    def test_explicit_cross_reference_three_2(self):
        print('-----------------------------------------------------')
        print('    MAIN TEST: Test explicit THREE cross reference 2 ')
        print('-----------------------------------------------------')
        file_name_in = TEST_FOLDER_CATEGORIES+"cr_2_explicit_cross_reference_three_2.in"
        file_name_out = TEST_FOLDER_CATEGORIES+"cr_2_explicit_cross_reference_three_2.out"

        category_collection = CategoryCollection()
        self.__test_database_rebuild(category_collection)
        self.__test_emptied_folders()
        category_collection.build_nodes_from(file_name_in)

        fetch_query = ("select from_code, to_code, "
                        "explicit_cross_reference, pending "
                        "from elemental_categorycrossreference order by id;")
      
        category_collection.connect()
        category_collection.cursor.execute(fetch_query)
        results = category_collection.cursor.fetchall()

        with open(file_name_out) as f:
            data = json.load(f)
            # validate same amount of query rows.
            print(data)
            self.assertEqual(len(results), len(data))
            # second, compare each element
            for i, result in enumerate(results):
                self.assertEqual(result, tuple(data[i].values()))
        category_collection.close()


    def test_explicit_cross_reference_three_2_ordered(self):
        print('-------------------------------------------------------------')
        print('    MAIN TEST: Test explicit THREE cross reference 2 ORDERED ')
        print('-------------------------------------------------------------')
        file_name_in = TEST_FOLDER_CATEGORIES+"cr_2_explicit_cross_reference_three_2_ordered.in"
        file_name_out = TEST_FOLDER_CATEGORIES+"cr_2_explicit_cross_reference_three_2_ordered.out"

        category_collection = CategoryCollection()
        self.__test_database_rebuild(category_collection)
        self.__test_emptied_folders()
        category_collection.build_nodes_from(file_name_in)

        fetch_query = ("select from_code, to_code, "
                        "explicit_cross_reference, pending "
                        "from elemental_categorycrossreference order by id;")
      
        category_collection.connect()
        category_collection.cursor.execute(fetch_query)
        results = category_collection.cursor.fetchall()

        with open(file_name_out) as f:
            data = json.load(f)
            # validate same amount of query rows.
            print(data)
            self.assertEqual(len(results), len(data))
            # second, compare each element
            for i, result in enumerate(results):
                self.assertEqual(result, tuple(data[i].values()))
        category_collection.close()


    def test_explicit_cross_reference_three_3(self):
        print('-------------------------------------------------------------')
        print('    MAIN TEST: Test explicit THREE cross reference 3 ')
        print('-------------------------------------------------------------')
        file_name_in = TEST_FOLDER_CATEGORIES+"cr_2_explicit_cross_reference_three_3.in"
        file_name_out = TEST_FOLDER_CATEGORIES+"cr_2_explicit_cross_reference_three_3.out"

        category_collection = CategoryCollection()
        self.__test_database_rebuild(category_collection)
        self.__test_emptied_folders()
        category_collection.build_nodes_from(file_name_in)

        fetch_query = ("select from_code, to_code, "
                        "explicit_cross_reference, pending "
                        "from elemental_categorycrossreference order by id;")
      
        category_collection.connect()
        category_collection.cursor.execute(fetch_query)
        results = category_collection.cursor.fetchall()

        with open(file_name_out) as f:
            data = json.load(f)
            # validate same amount of query rows.
            print(data)
            self.assertEqual(len(results), len(data))
            # second, compare each element
            for i, result in enumerate(results):
                self.assertEqual(result, tuple(data[i].values()))
        category_collection.close()


    def test_explicit_cross_reference_more_1(self):
        print('-------------------------------------------------------')
        print('    MAIN TEST: Test explicit MORE cross reference 1    ')
        print('-------------------------------------------------------')
        file_name_in = TEST_FOLDER_CATEGORIES+"cr_2_explicit_cross_reference_more_1.in"
        file_name_out = TEST_FOLDER_CATEGORIES+"cr_2_explicit_cross_reference_more_1.out"

        category_collection = CategoryCollection()
        self.__test_database_rebuild(category_collection)
        self.__test_emptied_folders()
        category_collection.build_nodes_from(file_name_in)

        fetch_query = ("select from_code, to_code, "
                        "explicit_cross_reference, pending "
                        "from elemental_categorycrossreference order by id;")
      
        category_collection.connect()
        category_collection.cursor.execute(fetch_query)
        results = category_collection.cursor.fetchall()

        with open(file_name_out) as f:
            data = json.load(f)
            # validate same amount of query rows.
            print(data)
            self.assertEqual(len(results), len(data))
            # second, compare each element
            for i, result in enumerate(results):
                self.assertEqual(result, tuple(data[i].values()))
        category_collection.close()


    def test_explicit_cross_reference_more_2(self):
        print('-------------------------------------------------------')
        print('    MAIN TEST: Test explicit MORE cross reference 2    ')
        print('-------------------------------------------------------')
        file_name_in = TEST_FOLDER_CATEGORIES+"cr_2_explicit_cross_reference_more_2.in"
        file_name_out = TEST_FOLDER_CATEGORIES+"cr_2_explicit_cross_reference_more_2.out"

        category_collection = CategoryCollection()
        self.__test_database_rebuild(category_collection)
        self.__test_emptied_folders()
        category_collection.build_nodes_from(file_name_in)

        fetch_query = ("select from_code, to_code, "
                        "explicit_cross_reference, pending "
                        "from elemental_categorycrossreference order by id;")
      
        category_collection.connect()
        category_collection.cursor.execute(fetch_query)
        results = category_collection.cursor.fetchall()

        with open(file_name_out) as f:
            data = json.load(f)
            # validate same amount of query rows.
            print(data)
            self.assertEqual(len(results), len(data))
            # second, compare each element
            for i, result in enumerate(results):
                self.assertEqual(result, tuple(data[i].values()))
        category_collection.close()


    def test_explicit_cross_reference_more_3(self):
        print('-------------------------------------------------------')
        print('    MAIN TEST: Test explicit MORE cross reference 3    ')
        print('-------------------------------------------------------')
        file_name_in = TEST_FOLDER_CATEGORIES+"cr_2_explicit_cross_reference_more_3.in"
        file_name_out = TEST_FOLDER_CATEGORIES+"cr_2_explicit_cross_reference_more_3.out"

        category_collection = CategoryCollection()
        self.__test_database_rebuild(category_collection)
        self.__test_emptied_folders()
        category_collection.build_nodes_from(file_name_in)

        fetch_query = ("select from_code, to_code, "
                        "explicit_cross_reference, pending "
                        "from elemental_categorycrossreference order by id;")
      
        category_collection.connect()
        category_collection.cursor.execute(fetch_query)
        results = category_collection.cursor.fetchall()

        with open(file_name_out) as f:
            data = json.load(f)
            # validate same amount of query rows.
            print(data)
            self.assertEqual(len(results), len(data))
            # second, compare each element
            for i, result in enumerate(results):
                self.assertEqual(result, tuple(data[i].values()))
        category_collection.close()
    
    def test_explicit_cross_reference_more_4(self):
        print('-------------------------------------------------------')
        print('    MAIN TEST: Test explicit MORE cross reference 4    ')
        print('-------------------------------------------------------')
        file_name_in = TEST_FOLDER_CATEGORIES+"cr_2_explicit_cross_reference_more_4.in"
        file_name_out = TEST_FOLDER_CATEGORIES+"cr_2_explicit_cross_reference_more_4.out"

        category_collection = CategoryCollection()
        self.__test_database_rebuild(category_collection)
        self.__test_emptied_folders()
        category_collection.build_nodes_from(file_name_in)

        fetch_query = ("select from_code, to_code, "
                        "explicit_cross_reference, pending "
                        "from elemental_categorycrossreference order by id;")
      
        category_collection.connect()
        category_collection.cursor.execute(fetch_query)
        results = category_collection.cursor.fetchall()

        with open(file_name_out) as f:
            data = json.load(f)
            # validate same amount of query rows.
            print(data)
            self.assertEqual(len(results), len(data))
            # second, compare each element
            for i, result in enumerate(results):
                self.assertEqual(result, tuple(data[i].values()))
        category_collection.close()
    

    def test_content_and_explicit_cross_reference(self):
        print('---------------------------------------------------------')
        print('    MAIN TEST: Test content and explicit cross reference ')
        print('---------------------------------------------------------')
        file_name_in = TEST_FOLDER_CATEGORIES+"cr_3_content_AND_explicit_cross_reference.in"
        file_name_out = TEST_FOLDER_CATEGORIES+"cr_3_content_AND_explicit_cross_reference.out"

        category_collection = CategoryCollection()
        self.__test_database_rebuild(category_collection)
        self.__test_emptied_folders()
        category_collection.build_nodes_from(file_name_in)

        fetch_query = ("select from_code, to_code, content_cross_reference, "
                        "explicit_cross_reference, pending "
                        "from elemental_categorycrossreference order by id;")
      
        category_collection.connect()
        category_collection.cursor.execute(fetch_query)
        results = category_collection.cursor.fetchall()

        with open(file_name_out) as f:
            data = json.load(f)
            # validate same amount of query rows.
            print(data)
            self.assertEqual(len(results), len(data))
            # second, compare each element
            for i, result in enumerate(results):
                self.assertEqual(result, tuple(data[i].values()))
        category_collection.close()
    

    def test_content_and_explicit_cross_reference_simple(self):
        print('---------------------------------------------------------')
        print('    MAIN TEST: Test content and explicit cross reference simple')
        print('---------------------------------------------------------')
        file_name_in = TEST_FOLDER_CATEGORIES+"cr_3_content_AND_explicit_cross_reference_simple.in"
        file_name_out = TEST_FOLDER_CATEGORIES+"cr_3_content_AND_explicit_cross_reference.out"

        category_collection = CategoryCollection()
        self.__test_database_rebuild(category_collection)
        self.__test_emptied_folders()
        category_collection.build_nodes_from(file_name_in)

        fetch_query = ("select from_code, to_code, content_cross_reference, "
                        "explicit_cross_reference, pending "
                        "from elemental_categorycrossreference order by id;")
      
        category_collection.connect()
        category_collection.cursor.execute(fetch_query)
        results = category_collection.cursor.fetchall()

        with open(file_name_out) as f:
            data = json.load(f)
            # validate same amount of query rows.
            print(data)
            self.assertEqual(len(results), len(data))
            # second, compare each element
            for i, result in enumerate(results):
                self.assertEqual(result, tuple(data[i].values()))
        category_collection.close()

if __name__ == '__main__':
    unittest.main()