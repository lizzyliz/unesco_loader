import os
import sys
import unittest
import psycopg2
import json
import re

from pathlib import Path
from pprint import pprint

BOOKMARKING_FOLDER = str(Path(__file__).resolve().parent.parent.parent)
sys.path.append(BOOKMARKING_FOLDER)
from main_route_file import BM_MODULES_DIR, UNESCO_DIR
sys.path.append(BM_MODULES_DIR)
sys.path.append(UNESCO_DIR)


from unesco_loader.database_connection import DatabaseConnection
from unesco_loader.category_collection import CategoryCollection
from folders_config import FOLDERS, UNESCO_REFERENCES

class TestCrossReferencesIntegrity(unittest.TestCase):
    """ Tests that are made to any environment after loading categories.
    """
    def __init__(self, *args, **kwargs):
        self.gral_cross_references = {}
        super(TestCrossReferencesIntegrity, self).__init__(*args, **kwargs)


    def __load_file_references(self):
        print('here')
        cc = CategoryCollection()
        file_route = UNESCO_REFERENCES
        references_by_code = {}

        with open(file_route) as file:
            line = file.readline()
            while line:
                '''
                category_data = {
                    'code': string_code,
                    'name': category_name,
                    'file_name': category_file_name,
                    'cross': cross_categories_codes,
                }
                '''
                category_data = cc.line_to_category_data(line)
                pprint(category_data)
                references_by_code[category_data['code']] = {
                    'name': category_data['name'],
                    'file_name': category_data['file_name'],
                    'cross_codes': category_data['cross']
                }
                line = file.readline()
        print('out')
        del cc ######
        print('out 2')
        return references_by_code


    def __load_cross_references(self, url_results, exp_c_codes, db_connection):
        cross_references = []
        ### interpolating both.
        if url_results is not None:
            # url cross reference category code
            for url_tuple in url_results:
                url_c_id    = url_tuple[0]
                url_c_code  = url_tuple[1]
                url_c_name  = url_tuple[2]
                # there are cross references by code and by url.
                if url_c_code in exp_c_codes:
                    exp_c = True
                    url_c = True
                    exp_c_codes.pop(exp_c_codes.index(url_c_code))
                else:
                # only cross reference by url.
                    exp_c = False
                    url_c = True
                # add data to cross_reference_data, both ids exist.
                cross_reference_data = {
                    'from_code' : c_code,
                    'to_code'   : url_c_code,
                    'from_id'   : c_id,
                    'to_id'     : url_c_id,
                    'content'   : url_c,
                    'explicit'  : exp_c,
                    'pending'   : False
                }
                cross_references.append(cross_reference_data)

        if (exp_c_codes is not None) and (len(exp_c_codes)>0):
            # only cross reference by explicit mention in file.
            for explicit_code in exp_c_codes:
                existance_query = ("select id from elemental_category "
                    "where code = "+str(explicit_code)+";")
                db_connection.cursor.execute(existance_query)
                existance_result = db_connection.cursor.fetchone()

                if existance_result is not None:
                    explicit_id = existance_result[0]
                    pending = False
                else:
                    explicit_id = None
                    pending = True

                cross_reference_data = {
                    'from_code' : c_code,
                    'to_code'   : explicit_code,
                    'from_id'   : c_id,
                    'to_id'     : explicit_id,
                    'content'   : False,
                    'explicit'  : True,
                    'pending'   : pending,
                }
                cross_references.append(cross_reference_data)
        # else:
        #     # no cross references. Next.
        #     continue
        return cross_references


    def __load_db_cross_references(self, db_connection):
        cross_query = (" select id, from_code, to_code, content_cross_reference, "
                        "explicit_cross_reference, "
                        "from_category_id, to_category_id, pending from "
                        "elemental_categorycrossreference order by id; ")
        db_connection.cursor.execute(cross_query)
        cross_results = db_connection.cursor.fetchall()
        for cross_result in cross_results:
            id = cross_result[0]
            self.gral_cross_references[id] = {
                'checked'   : False,
                'from_code' : cross_result[1],          
                'to_code'   : cross_result[2],          
                'content'   : cross_result[3],          
                'explicit'  : cross_result[4],
                'from_id'   : cross_result[5],
                'to_id'     : cross_result[6],
                'pending'   : cross_result[7],
            }


    def __verify_and_check(cross_reference_data):
        for cr_id in self.gral_cross_references:
            if cross_reference_data['from_code'] == self.gral_cross_reference[cr_id]['from_code']:
               # keep data the same way
               print('ok')
            elif cross_reference_data['to_code'] == self.gral_cross_reference[cr_id]['from_code']:
               # exchange data, no puedo intercalar asi no mas.
               # creo que si puedo.
               from_code_aux = cross_reference_data['from_code']
               from_id_aux = cross_reference_data['from_id']
               cross_reference_data['from_code'] = cross_reference_data['to_code']
               cross_reference_data['from_id'] = cross_reference_data['to_id']
               cross_reference_data['to_code'] = from_code_aux
               cross_reference_data['to_id'] = from_id_aux
            else:
                continue
            self.assertEqual(self.gral_cross_reference[cr_id]['from_code'], cross_reference_data['from_code'])
            self.gral_cross_reference[cr_id]['checked'] = True


    def test_cross_references_integrity(self):
        unesco_references = self.__load_file_references()
        category_query = ("select id, code, name, url_content from elemental_category;")
        print('ok first')
        self.cursor.execute(query)
        db_connection = DatabaseConnection()
        db_connection.connect()
        db_connection.cursor.execute(category_query)
        results = db_connection.cursor.fetchall()
        self.__load_db_cross_references(db_connection)

        for category_data in results:
            c_id    = category_data[0]
            c_code  = category_data[1]
            c_name  = category_data[2]
            c_url   = category_data[3]
            exp_c_codes = None
            
            ### cross references by file explicit mention. ###
            if c_code in unesco_references:
                exp_c_codes =  unesco_references[c_code]['cross_codes']

            ### cross references by url. ###
            url_query = ("select id, code, name from "
                "elemental_category where url_content = '"+c_url+"' "
                "except select id from elemental_category where id="+str(c_id)+";")
            db_connection.cursor.execute(url_query)
            url_results = db_connection.cursor.fetchall()

            cross_references = []
            cross_references = self.__load_cross_references(url_results, exp_c_codes, db_connection)

            for cross_reference_data in cross_references:
                self.__verify_and_check(cross_reference_data)

        pprint(self.gral_cross_references)
        db_connection.close()
    
    
if __name__ == '__main__':
    unittest.main()
