import os
import sys
import unittest
import psycopg2
import json
import re

from pathlib import Path
from pprint import pprint

BOOKMARKING_FOLDER = str(Path(__file__).resolve().parent.parent.parent)
sys.path.append(BOOKMARKING_FOLDER)
from main_route_file import BM_MODULES_DIR, UNESCO_DIR
sys.path.append(BM_MODULES_DIR)
sys.path.append(UNESCO_DIR)


from unesco_loader.database_connection import DatabaseConnection
from unesco_loader.category_collection import CategoryCollection
from folders_config import FOLDERS

class TestAfterLoadCategories(unittest.TestCase):
    """ Tests that are made to any environment after loading categories.
    """

    def __sum_total_words_in_file(self, file_route):
        with open(file_route) as r_file:
            total_in_file = 0
            for line in r_file:
                #print(line)
                match = re.match(r"\"(?P<word>\w+)\":(?P<freq>[0-9]+)", line)
                # match = re.match(r"(?P<freq>[0-9]+)", line)
                total_in_file += int(match['freq'])
            print("Total in file: "+str(total_in_file))
        return total_in_file
            

    def test_total_words(self):
        """ Total word counts for each word in frequencies folder, 
        summed to verify total_words count in database. Both should
        be equal.
        """
        self.db_connection = DatabaseConnection()
        self.db_connection.connect()

        frequencies_folder = FOLDERS['frequencies_folder']
        # for each file, verify in database
        for file in os.listdir(frequencies_folder): 
            file_route = frequencies_folder+file
            total_in_file = self.__sum_total_words_in_file(file_route)
            category_file_name = os.path.splitext(file)[0]
            category_name = category_file_name.replace('_', ' ')
            if not (category_name.startswith('old')):
                query = ("select total_words from elemental_category "
                    "where name = '"+category_name+"';")
                print(query)
                self.db_connection.cursor.execute(query)
                result = self.db_connection.cursor.fetchone()
                print("Total in database: "+str(result[0]))
                self.assertEqual(result[0], total_in_file)
        print()


    def test_categories_names(self):
        def word_has_this_letters(word):
            english_alphabet = ["a","b","c","d","e","f","g","h","i","j","k",
            "l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]
            spanish_added_letters = ['á', 'é', 'í', 'ó', 'ú', 'ü', 'ñ']
            other_characters = [ ' ', '(', ')', '.']
            # print('english:')
            # pprint(english_alphabet)
            for char in word:
                if not((char in english_alphabet) or 
                    (char in spanish_added_letters) or
                    (char in other_characters)):
                    # print(char)
                    return False
            return True

        self.db_connection = DatabaseConnection()
        self.db_connection.connect()
        query = "select name from elemental_category;"
        self.db_connection.cursor.execute(query)
        for register_tuple in self.db_connection.cursor:
            print(register_tuple[0])
            self.assertTrue(word_has_this_letters(register_tuple[0]))
        print()

    
    def test_root_total_words(self):
        # cc stands for category_collection
        frequencies_folder = FOLDERS['frequencies_folder']
        # As they are tests in controlled environments, before testing 
        # this test, recalibrate action in categories_manager should be 
        # executed manually from console before executing this test.
        def test_sum_root_sub_total_words_files(cc, category_id):
            children_query = ("select id from elemental_category "
                "where parent_category_id ="+str(category_id)+";")
            cc.cursor.execute(children_query)
            children = cc.cursor.fetchall()

            # To verify, first obtain data from DB
            self_query = ("select name, total_words from elemental_category "
                "where id ="+str(category_id)+";")
            cc.cursor.execute(self_query)
            self_query_result = cc.cursor.fetchone()
            db_total_words = self_query_result[1]
            file_name = self_query_result[0].replace(' ', '_')+'.frq'
            file_route = frequencies_folder+file_name

            if (len(children) > 0):
                sum_children_file_total_words = 0
                for db_tuple in children:
                    sub_category_id = db_tuple[0]
                    child_total_words = test_sum_root_sub_total_words_files(cc, sub_category_id) ####
                    sum_children_file_total_words += child_total_words
                # should be added the words counting for the category's old file.
                old_file_route = frequencies_folder+'old_'+file_name
                print("Total words for old category file...")
                self_old_total_words_file = self.__sum_total_words_in_file(old_file_route)
                sum_children_file_total_words += self_old_total_words_file
            else:
                sum_children_file_total_words = self.__sum_total_words_in_file(file_route)

            print("Total words for: "+self_query_result[0])
            print("db: "+str(db_total_words)+", sub_files: "+str(sum_children_file_total_words))
            self.assertEqual(db_total_words, sum_children_file_total_words)
            # return file_total_words
            return sum_children_file_total_words

        cc = CategoryCollection()
        cc.connect()
        root_ids = cc.get_children_ids_for()
        for root_id in root_ids:
            # root_total_words = test_sum_root_sub_total_words_files(cc, root_id[0])
            test_sum_root_sub_total_words_files(cc, root_id[0])
        cc.close()
    
    
if __name__ == '__main__':
    unittest.main()
