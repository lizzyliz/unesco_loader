#!/usr/bin/python3.6.0
# -*- coding: utf-8 -*-
# python test_load_categories.py TestLoadCategories.test_root_with_children
import os
import sys
import unittest
import psycopg2
import json
import time

from pathlib import Path
from pprint import pprint

BOOKMARKING_FOLDER = str(Path(__file__).resolve().parent.parent.parent)
sys.path.append(BOOKMARKING_FOLDER)
from main_route_file import BM_MODULES_DIR, UNESCO_DIR
sys.path.append(BM_MODULES_DIR)
sys.path.append(UNESCO_DIR)

from  categories_manager import delete_folders_content
from  database_connection import DatabaseConnection
from category_collection import CategoryCollection



from folders_config import FOLDERS, TEST_FOLDER_CATEGORIES

class TestLoadCategories(unittest.TestCase):
    """ 
    Tests are made directly by calling modules functions or objects, 
    NOT by console interaction.
    Before every load_categories execution, database should be rebuild 
    and folders with generated files should be emptied.
    """
    def __test_database_rebuild(self, category_collection):
        print('----->  Inner testing: Test database rebuild. ---')
        # setting environment for test.
        tables_so_far = [
            'aggrupations_administrator',
            'aggrupations_affiliation',
            'aggrupations_bmgroup',
            'aggrupations_bmreference',
            'aggrupations_invitation',
            'aggrupations_member',
            'aggrupations_membertaskpermission',
            'aggrupations_participant',
            'aggrupations_solicitude',
            'aggrupations_solicitudestatus',
            'aggrupations_taskpermission', # 11 tables for aggrupations
            'auth_group', 
            'auth_group_permissions', 
            'auth_permission', 
            'auth_user', 
            'auth_user_groups', 
            'auth_user_user_permissions', # 6 tables for auth 
            'django_admin_log', 
            'django_content_type', 
            'django_migrations', 
            'django_session', # 4 tables for django
            'elemental_bmuser',
            'elemental_category', 
            'elemental_categorycrossreference', 
            'elemental_owner',
            'elemental_reference', 
            'elemental_reference_tags',
            'elemental_tag',
            'elemental_webpage', 
            'elemental_webpagecategories', # 9 tables for elemental
            'social_auth_association', 
            'social_auth_code', 
            'social_auth_nonce', 
            'social_auth_partial', 
            'social_auth_usersocialauth', # 5 tables for social auth

        ]
        query_database_tables = ( "select tablename from pg_catalog.pg_tables "
                                "WHERE schemaname != 'pg_catalog' "
                                "AND schemaname != 'information_schema' "
                                "ORDER BY tablename;")
        tables_to_ommit = [
            'auth_permission',
            'django_content_type',
            'django_migrations'
        ]
        category_collection.rebuild()
        category_collection.connect()
        # Then validate results in two steps.
        category_collection.cursor.execute(query_database_tables)
        results = category_collection.cursor.fetchall()
        # first validate same amount of tables.
        self.assertEqual(len(results), len(tables_so_far))
        # second, compare each table element existance.
        # third, validate that each table is empty.
        base_count_query = "SELECT COUNT(*) FROM "
        for i, result in enumerate(results):
            self.assertEqual(tables_so_far[i], result[0])
            print('Table: '+tables_so_far[i])
            if not(tables_so_far[i] in tables_to_ommit):
                query = base_count_query+tables_so_far[i]+";"
                category_collection.cursor.execute(query)
                tuples_in_table = category_collection.cursor.fetchone()
                self.assertEqual(tuples_in_table[0], 0)
        category_collection.close()


    def __test_emptied_folders(self):
        print('----->  Inner testing: Test emptied folders. ---')
        delete_folders_content()
        for key, folder in FOLDERS.items():
            self.assertTrue((os.listdir(folder) == []))

    def test_codes(self):
        print('--------------------------------------------')
        print('    MAIN TEST: Test categories codes        ')
        print('--------------------------------------------')
        file_name_in = TEST_FOLDER_CATEGORIES+"cl_1_unesco_codes.in"
        file_name_out = TEST_FOLDER_CATEGORIES+"cl_1_unesco_codes.out"

        category_collection = CategoryCollection()
        self.__test_database_rebuild(category_collection)
        self.__test_emptied_folders()
        category_collection.build_nodes_from(file_name_in)

        fetch_query = ("select name, code, root_code, trail_code "
                        "FROM elemental_category ORDER BY id;")
        category_collection.connect()
        category_collection.cursor.execute(fetch_query)
        db_results = category_collection.cursor.fetchall()

        with open(file_name_out) as f:
            output_file_data = json.load(f)
            # first, validate same amount of query rows.
            print(output_file_data)
            self.assertEqual(len(db_results), len(output_file_data))
            # do not sort items, they should be named in the same order
            # as they were mentioned in the input file, instead just compare
            for i, db_result in enumerate(db_results):
                pprint(db_result)
                pprint(tuple(output_file_data[i].values()))
                self.assertEqual(db_result, tuple(output_file_data[i].values()))
        category_collection.close()


    def test_only_root_categories(self):
        print('--------------------------------------------')
        print('    MAIN TEST: Test only root categories    ')
        print('--------------------------------------------')
        file_name_in = TEST_FOLDER_CATEGORIES+"cl_2_roots_without_children.in"
        file_name_out = TEST_FOLDER_CATEGORIES+"cl_2_roots_without_children.out"

        category_collection = CategoryCollection()
        self.__test_database_rebuild(category_collection)
        self.__test_emptied_folders()
        category_collection.build_nodes_from(file_name_in)

        fetch_query = ("select c1.code, c1.root_code, c1.trail_code, c1.name, c2.code "
                        "FROM elemental_category c1 left outer JOIN elemental_category c2 "
                        "ON c1.parent_category_id = c2.id "
                        "ORDER BY c1.code;")
        category_collection.connect()
        category_collection.cursor.execute(fetch_query)
        results = category_collection.cursor.fetchall()

        with open(file_name_out) as f:
            data = json.load(f)
            # validate same amount of query rows.
            print(type(data))
            self.assertEqual(len(results), len(data))
            # sort items to compare
            new_data = sorted(data, key=lambda k: k['code']) 
            # second, compare each element
            for i, result in enumerate(results):
                pprint(result)
                pprint(tuple(new_data[i].values()))
                self.assertEqual(result, tuple(new_data[i].values()))
        category_collection.close()


    def test_max_depth_unesco(self):
        print('--------------------------------------------')
        print('    MAIN TEST: Test max depth unesco        ')
        print('--------------------------------------------')
        file_name_in = TEST_FOLDER_CATEGORIES+"cl_3_max_depth_unesco.in"
        file_name_out = TEST_FOLDER_CATEGORIES+"cl_3_max_depth_unesco.out"

        category_collection = CategoryCollection()
        self.__test_database_rebuild(category_collection)
        self.__test_emptied_folders()
        category_collection.build_nodes_from(file_name_in)

        fetch_query = ("select c1.name, c2.name, c1.code, c1.root_code, c1.trail_code "
                        "FROM elemental_category c1 left outer JOIN elemental_category c2 "
                        "ON c1.parent_category_id = c2.id "
                        "ORDER BY c1.id;")

        category_collection.connect()
        category_collection.cursor.execute(fetch_query)
        results = category_collection.cursor.fetchall()

        with open(file_name_out) as f:
            data = json.load(f)
            # validate same amount of query rows.
            print(data)
            self.assertEqual(len(results), len(data))
            # second, compare each element
            for i, result in enumerate(results):
                self.assertEqual(result, tuple(data[i].values()))
        category_collection.close()


    def test_root_with_children(self):
        """
        It may rise a conflict with category: lógica and category: lógica general
        as they may have same content, reflecting content cross reference, BUT
        for the purpose of this test THAT should not matter in this test, although
        making them have the same content will create a category cross reference tuple
        in the database, not affecting to this test.
        """
        print('--------------------------------------------')
        print('    MAIN TEST: Test root with children      ')
        print('--------------------------------------------')
        file_name_in = TEST_FOLDER_CATEGORIES+"cl_4_root_with_children.in"
        file_name_out = TEST_FOLDER_CATEGORIES+"cl_4_root_with_children.out"

        category_collection = CategoryCollection()
        self.__test_database_rebuild(category_collection)
        self.__test_emptied_folders()
        category_collection.build_nodes_from(file_name_in)

        fetch_query = ("select c1.name, c2.name, c1.code, c1.root_code, c1.trail_code "
                        "FROM elemental_category c1 left outer JOIN elemental_category c2 "
                        "ON c1.parent_category_id = c2.id "
                        "ORDER BY c1.id;")

        category_collection.connect()
        category_collection.cursor.execute(fetch_query)
        results = category_collection.cursor.fetchall()

        with open(file_name_out) as f:
            data = json.load(f)
            # validate same amount of query rows.
            print(data)
            print()
            self.assertEqual(len(results), len(data))
            # second, compare each element
            for i, result in enumerate(results):
                self.assertEqual(result, tuple(data[i].values()))
        category_collection.close()


if __name__ == '__main__':
    unittest.main()
