#!/usr/bin/python3.6.0
# -*- coding: utf-8 -*-

import requests
import re
from pathlib import Path
from pprint import pprint
from shutil import copyfile

from text_processing import extract_text_from_html, frequencies_for_cleaned_text, dump_frequencies_tuple_in_file
from category_access import CategoryAccess
from cross_category import CrossCategory
from console_constants import colors as c


BASE_CODE_LENGTH = 4
JUMP_CODE_LENGTH = 2
MAX_CODE_LENGTH = BASE_CODE_LENGTH + JUMP_CODE_LENGTH + 1


class Category(CategoryAccess):
    # code, root_code, trail_code
    # name, url_content, title_content, description_content    
    # parent_category_id
    def __init__(self, data_hash, connection, cursor):
        '''
        all data from category_data must be string.
        This method doesn't validate if input text with codes and categories is 
        consistent. It assumes that the file codes are correctly hierarchized.
        All arguments must be treated as strings, from end to end. Including 
        those related to the code.
        '''
        super().__init__(connection, cursor)
        if 'id' not in data_hash.keys():
            # create
            user_category_data = self.web_user_access.get_user_category_data(
                data_hash['name'])
            if user_category_data is None:
                self.set_null()
            else:
                # save data in files sets self.total_words 
                self.save_data_in_files(data_hash, user_category_data) 
        
                self.name           = data_hash['name']
                self.url_content    = user_category_data['url']
                self.title_content  = user_category_data['title']
                self.description_content = user_category_data['description']
                self.code   =  data_hash['code']
                file_name   = data_hash['file_name']

                self.generate_root_trail_parent_codes()
                # it is necessary to save before cross categories.
                self.id = self.save_in_database() 
                # pprint(data_hash['cross'])
                self.print_data()
                cross_categories_data = self.evaluate_cross_categories(data_hash['cross'])
                for cross_category_data in cross_categories_data:
                    cross_category = CrossCategory(cross_category_data, connection, cursor)
        else: 
            # obtain
            category_tuple = self.get_category_data_by_id(data_hash['id'])
            if category_tuple is not None:
                # pprint(category_tuple)
                self.id             = category_tuple[0]
                self.code           = category_tuple[1]
                self.root_code      = category_tuple[2]
                self.trail_code     = category_tuple[3]
                self.name           = category_tuple[4]
                self.url_content    = category_tuple[5]
                self.title_content  = category_tuple[6]
                self.description_content    = category_tuple[7]
                self.total_words            = category_tuple[8]
                self.parent_category_id     = category_tuple[9]
            else:
                self.set_null()


    def set_null(self):
        self.name = None
        self.code = self.root_code = self.trail_code = None
        self.url_content = self.title_content= self.description_content = None
        self.parent_category_id = None


    def save_text_data(self, file_name):
        segmented_text = extract_text_from_html(file_name, self.RAW_HTML_FOLDER)
        if segmented_text is None: return None

        text_file = open(self.TEXT_FOLDER+file_name+'.txt', "w")
        for word in segmented_text:
            text_file.write(word)
        text_file.close()
        return segmented_text


    def save_html_data(self, file_name, url):
        file_route = self.RAW_HTML_FOLDER+file_name+'.html'
        if Path(file_route).is_file():
            print('File already exists.')
            return None
        try:
            file = open(file_route, 'w+')
            response = requests.get(url)
            file.write(response.text)
            file.close()
        except:
            print('An error has ocurred while saving html data.')
        # return file_name



    # unesco_data: {code, name, file_name, cross}
    # user_data : {url, title, description}
    # user_input_url_to_html(search_results, terms_to_search, file_name, raw_html_folder)
    def save_data_in_files(self, unesco_data, user_data):
        # save html data obtains data with request library usage.
        self.save_html_data(unesco_data['file_name'], user_data['url'])
        # save text data uses extract text from html from text processing.
        text = self.save_text_data(unesco_data['file_name'])
        # directly use frequencies for cleaned text from text processing.
        self.total_words, frequencies_tuple = frequencies_for_cleaned_text(text, unesco_data['file_name'],
            self.CLEAN_TEXT_FOLDER)
        dump_frequencies_tuple_in_file(frequencies_tuple, unesco_data['file_name'], self.FREQUENCIES_FOLDER)


    def data_to_dict(self):
        node_data = {
            'root_code' : self.root_code, # string
            'trail_code': self.trail_code, # string 
            'code'      : self.code, # integer
            'name'      : self.name, # string 
            'url_content'       : self.url_content, # string 
            'title_content'     : self.title_content, # string 
            'description_content'   : self.description_content, # string 
            'total_words'           : self.total_words, # integer
            'parent_category_id'    : self.parent_category_id, # integer
        }
        return node_data


    #private
    def save_in_database(self):
        node_data = self.data_to_dict()
        print(c['blue']+"*** creating node ***"+c['end'])
        fields = self.dictionary_steamroller(node_data.keys())
        values = node_data.values()
        array_values = []
        for v in values: array_values.append(v)
        self.register_category_in_table(fields, array_values)
        return self.get_category_id_by_code(self.code)

    def update_total_words(self):
        print("updating total words: "+str(self.total_words)+" for :"+self.name)
        self.update_category_total_words(self.total_words, self.id)

    # private
    def generate_root_trail_parent_codes(self):
        code_len    = len(self.code)
        if code_len == BASE_CODE_LENGTH:
            self.root_code   = self.code[:2]
            if self.code.endswith('00'):
                # create root node.
                self.trail_code  = '00'
                self.parent_category_id= None
            else:
                # children of root node, assuming they are secuentially defined.
                self.trail_code  = self.code[2:]
                # ***
                self.parent_category_id = self.get_category_id_by_code(self.root_code+'00')
        elif code_len == (BASE_CODE_LENGTH+JUMP_CODE_LENGTH):
            self.root_code   = self.code[:4]
            self.trail_code  = self.code[4:]
            self.parent_category_id = self.get_category_id_by_code(self.root_code)
        elif code_len == MAX_CODE_LENGTH : 
            self.root_code   = self.code[:6]
            self.trail_code  = self.code[6]
            self.parent_category_id = self.get_category_id_by_code(self.root_code)
        else:
            # there's a wrong definition in the code.
            print("wrong definition in UNESCO code.\nLeast size of code is "+ 
                BASE_CODE_LENGTH)
            print("No node was created for this category definition: "+ 
                self.code +" " +self.name)
            self.set_null()


    # private
    def get_categories_by_url_except_own(self):
        '''Returns list of categories except same one.
        '''
        categories = []
        categories_ids = self.get_categories_ids_with_url_except(self.url_content, self.id)
        for category_id in categories_ids:
            data_hash = {'id': category_id[0]}
            category = Category(data_hash, self.connection, self.cursor)
            categories.append(category)
        return categories


    #private
    def evaluate_cross_categories(self, mentioned_cross_codes):  
        # categories_by_url = self.get_categories_by_url_except_own()
        categories_codes_by_url = []
        union_cross_categories = []
        categories_codes = self.get_categories_codes_with_url_except(self.url_content, self.code)
        # pprint(categories_codes)
        for category_tuple in categories_codes: categories_codes_by_url.append(category_tuple[0])

        to_codes_cross_reference_types = []
        # verify content cross reference and explicit cross reference.                                
        for mentioned_cross_code in mentioned_cross_codes:
            if mentioned_cross_code in categories_codes_by_url:
                position = categories_codes_by_url.index(mentioned_cross_code)
                categories_codes_by_url.pop(position)
                content = True
            else: 
                content = False
            code_type = {
                'to_code': mentioned_cross_code,
                'content': content,
                'explicit': True,
            }
            to_codes_cross_reference_types.append(code_type)

        for category_code_by_url in categories_codes_by_url:
            code_type = {
                'to_code': category_code_by_url,
                'content': True,
                'explicit': False,
            }
            to_codes_cross_reference_types.append(code_type)       
        # pprint(to_codes_cross_reference_types)

        for cross_reference_type in to_codes_cross_reference_types:
            # first verify if "to_code" category as Category exists.
            to_category_id = self.get_category_id_by_code(cross_reference_type['to_code'])
            if to_category_id is None:  pending = True
            else:                       pending = False
            # create cross_reference_data
            cross_reference = {
                'from_category_id': self.id, 
                'from_code': int(self.code),
                'to_category_id': to_category_id,
                'to_code': int(cross_reference_type['to_code']),
                'content_cross_reference': cross_reference_type['content'],
                'explicit_cross_reference': cross_reference_type['explicit'],
                'pending': pending,
            }
            union_cross_categories.append(cross_reference)
            # then verify if cross_reference between codes exist.
            # The sentence mentioned above should be checked in cross_reference class.
        # pprint(union_cross_categories)
        return union_cross_categories
            

    #private, deprecated
    def old_evaluate_cross_categories(self, mentioned_cross_codes):  
        for category in categories_by_url:
            category.print_data()
            new_cross_reference = {
                'from_category_id': self.id,
                'from_code': self.code,
                'to_category_id': self.get_category_id_by_code(category.code),
                'to_code': category.code,
                'content_cross_reference': True,
                'explicit_cross_reference': False,
                'pending': self.get_category_id_by_code(category.code) is None,
            }
            if(category.code in mentioned_cross_codes):
                # content and explicit cross reference
                new_cross_reference['explicit_cross_reference'] = True
                pop_index = mentioned_cross_codes.index(category.code)
                mentioned_cross_codes.pop(pop_index)
            union_cross_categories.append(new_cross_reference)

        # implicit, no cross codes LEFT
        for cross_code in mentioned_cross_codes:
            new_cross_reference = {
                'from_category_id': self.id,
                'from_code': self.code,
                'to_category_id': self.get_category_id_by_code(cross_code),
                'to_code': cross_code,
                'content_cross_reference': False,
                'explicit_cross_reference': True,
                'pending': self.get_category_id_by_code(cross_code) is None,
            }
            union_cross_categories.append(new_cross_reference)
        return union_cross_categories


    def recalibrate(self):
        #recalibrate by children definition
        print('*** categoria: '+self.name)
        children_ids = self.get_children_ids_for(self.id)
        # print('showing children ids:')
        # pprint(children_ids)
        children_frequencies = []
        if len(children_ids) > 0:
            file_name = self.name.replace(' ', '_')
            old_file_name = 'old_'+file_name
            from_file = self.FREQUENCIES_FOLDER+file_name+'.frq'
            to_file =  self.FREQUENCIES_FOLDER+old_file_name+'.frq'
            copyfile(from_file, to_file)
            self_frequencies = self.load_frequencies()
            children_total_words = 0

            for child_tuple_id in children_ids:
                category_data = {'id': child_tuple_id[0]}
                child_category = Category(category_data, self.connection, self.cursor)
                #new_total = self.total_words + child_category.total_words
                # self.total_words = new_total
                # children_frequencies.append = child_category.recalibrate()
                child_frequencies = child_category.recalibrate()
                children_total_words += child_category.total_words
                # print('showing child frequencies:')
                # pprint(child_frequencies)
                for word in child_frequencies:
                    if word in self_frequencies:
                        new_frequency = self_frequencies[word] + child_frequencies[word]
                        self_frequencies[word] = new_frequency
                    else:
                        self_frequencies[word] = child_frequencies[word]
                sorted_tuple = [(i, self_frequencies[i]) for i in sorted(self_frequencies, key=self_frequencies.get, reverse=True)]
                # print('showing sorted tuple:')
                # pprint(sorted_tuple)
                dump_frequencies_tuple_in_file(sorted_tuple, file_name, self.FREQUENCIES_FOLDER)

            self.total_words = self.total_words + children_total_words
            self.update_total_words()
            print(' DONE ')
            return self.load_frequencies()
        else:
            frequencies = self.load_frequencies()
            # print()
            return frequencies


    def load_frequencies(self):
        frequencies = {}
        file_name = self.name.replace(' ', '_')+'.frq'
        file_route = self.FREQUENCIES_FOLDER+file_name
        # print('file_route: '+file_route)
        with open(file_route, 'r', encoding='utf-8') as file:
            line = file.readline()
            while line:
                match = re.match(r"\"(?P<word>\w+)\":(?P<frec>[0-9]+)", line)
                frequencies[match['word']] = int(match['frec'])
                line = file.readline()
        return frequencies
        
    # private
    def print_data(self):
        print(c['blue']+"-------------------------------------------"+c['end'])
        print("category data for: "+self.name)
        print(c['blue']+"-------------------------------------------"+c['end'])
        print(c['blue']+"*"+c['end']+" id: "+str(self.id))
        print(c['blue']+"*"+c['end']+" code: "+str(self.code))
        print(c['blue']+"*"+c['end']+" root_code: "+self.root_code)
        print(c['blue']+"*"+c['end']+" trail_code: "+self.trail_code)
        print(c['blue']+"*"+c['end']+" name: "+self.name)
        print(c['blue']+"*"+c['end']+" url_content: "+self.url_content)
        print(c['blue']+"*"+c['end']+" title_content: "+self.title_content)
        print(c['blue']+"*"+c['end']+" description_content: "+self.description_content)
        print(c['blue']+"*"+c['end']+" total_words: "+str(self.total_words))
        print(c['blue']+"*"+c['end']+" parent_category_id: "+str(self.parent_category_id))
        print(c['blue']+"-------------------------------------------"+c['end'])

    # public
    def get_id(self):
        return self.id

    def get_url(self):
        return self.url_content