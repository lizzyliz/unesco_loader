BASE_CODE_LENGTH = 4
JUMP_CODE_LENGTH = 2
MAX_CODE_LENGTH = BASE_CODE_LENGTH + JUMP_CODE_LENGTH + 1

class Category():
	# code, root_code, trail_code
	# name, url_content, title_content, description_content    
	# parent_category_id

	def __init__(self, data_hash, connection, action='create'):
		self.table_name = 'elemental_category'
		if action is 'create':
        	self.name 			= data_hash['name']
        	self.url_content 	= data_hash['url_content']
        	self.title_content 	= data_hash['title_content']
        	self.description_content = data_hash['description_content']
			self.code 	=  data_hash['code']
        	file_name 	= data_hash['file_name']
        	code_len 	= len(self.code)

        	if code_len == BASE_CODE_LENGTH:
        	    self.root_code   = self.code[:2]
        	    if self.code.endswith('00'):
        	        # create root node.
        	        self.trail_code  = '00'
        	        self.parent_category_id= None
        	    else:
        	        # children of root node, assuming they are secuentially defined.
        	        self.trail_code  = self.code[3:]
        	        # ***
        	        self.parent_category_id = self.get_category_id_by_code(self.root_code+'00')
        	elif code_len == (BASE_CODE_LENGTH+JUMP_CODE_LENGTH):
        	    self.root_code   = self.code[:4]
        	    self.trail_code  = self.code[4:]
        	    self.parent_category_id = self.get_category_id_by_code(self.root_code)
        	elif code_len == MAX_CODE_LENGTH : 
        	    self.root_code   = self.code[:6]
        	    self.trail_code  = self.code[6]
        	    self.parent_category_id = self.get_category_id_by_code(self.root_code)
        	else:
        	    # there's a wrong definition in the code.
        	    print("wrong definition in UNESCO code.\nLeast size of code is "+ BASE_CODE_LENGTH)
        	    print("No node was created for this category definition: "+ self.code +" " +self.name)
        	    self.set_null()


        # create node.
        node_data = {
            'root_code' : root_code, # string
            'self.trail_code': self.trail_code, # string 
            'code'      : self.code, # integer
            'name'      : s_category_name, # string 
            'url_content'       : h_user_selected_data['url'], # string 
            'title_content'     : h_user_selected_data['title'], # string 
            'description_content': h_user_selected_data['description'], # string 
            'parent_category_id'   : parent_category, # integer
        }
        print("creating node")
        self.create_category(node_data)

        categories_with_url = self.find_url_in_categories(h_user_selected_data['url'])
        if categories_with_url or (len(category_data['cross']) > 0):
            self.validate_cross_category(categories_with_url, h_user_selected_data)




		if action is 'get_by':
			

		fields = self.get_fields_from_hash(category_data_hash)

	# private
	def set_null(self):
		self.name =	None
		self.code = self.root_code = self.trail_code = None
		self.url_content = self.title_content= self.description_content = None
        self.parent_category_id = None

	# private
	def get_query_creation(self, fields, ):
		query = ("INSERT INTO "+self.table_name+" "+fields+" VALUES \
            (%s, %s, %s, %s, %s, %s, %s, %s)", array_values)

	# private
	def get_fields_from_hash(self, data_hash):



    #private
    def dictionary_steamroller(self, dict_tuple, string_scape = False):
        # print(type(dict_tuple))
        s = '('
        pprint(dict_tuple)
        for element in dict_tuple:
            if string_scape and isinstance(element, str):
                s+="'"+element+"'"+', '
            elif element is None: 
                s+='NULL, '
            else:
                s+=element+', '
        final_s = s[:-2]+')'
        return final_s

            #private
    def create_category(self, node_data_hash):
        table_name = 'elemental_category'
        fields = self.dictionary_steamroller(node_data_hash.keys())
        values = node_data_hash.values()
        array_values = []
        for v in values:
            array_values.append(v)

        # values = self.dict_steamroller(node_data_hash.values(), True)

        #  self.cursor.execute("INSERT INTO "+table_name+" "+fields+" VALUES \
        #      (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s);" %( 
        #  node_data_hash['code'],
        #  node_data_hash['root_code'],
        #  node_data_hash['trail_code'],
        #  node_data_hash['name'],
        #  node_data_hash['url_content'],
        #  node_data_hash['title_content'],
        #  node_data_hash['description_content'],
        #  node_data_hash['parent_category_id']
        #      ))
        #  self.connection.commit()

        # self.cursor.execute("INSERT INTO "+table_name+" "+fields+" VALUES \
        #     (%%s, %%s, %%d, %%s, %%s, %%s, %%s, %%s, %%s, %%s)" %node_data_hash.values())

        self.db_connection.cursor.execute("INSERT INTO "+table_name+" "+fields+" VALUES \
            (%s, %s, %s, %s, %s, %s, %s, %s)", array_values)
        self.db_connection.connection.commit()

        	   # private
    def create_cross_category(self, ):


    #private
    def validate_cross_category(self, categories_with_url, h_user_selected_data):
        if len(categories_with_url)>0:
            if len(category_data['cross']) > 0:
                # explicit
                for category_with_url in categories_with_url:

                # for cross_code in category_data['cross']:
                    #register cross reference




            else:
                # implicit, no cross codes
                for category_with_url in categories_with_url:
                    # id, code, name, parent_category_id 
                    cross_category_data = {
                        'from_category' : 
                        'from_code' :      
                        'to_category' :    
                        'to_code' :        
                        'content_cross_reference' : 
                        'explicit_cross_reference' :
                        'pending' :  

                    }
                    self.create_cross_category()
                    category_with_url

            # possible cross reference. Ask user.
            h_user_selected_data['url']
            cross_categories = []
            for category in categories:
                cross_category = {
                    
                }