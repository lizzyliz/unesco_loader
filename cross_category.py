#!/usr/bin/python3.6.0
# -*- coding: utf-8 -*-
from cross_category_access import CrossCategoryAccess
from console_constants import colors as c
from pprint import pprint

class CrossCategory(CrossCategoryAccess):

    def __init__(self, data_hash, connection, cursor):
        super().__init__(connection, cursor)
        category_code_a = data_hash['from_code']
        category_code_b = data_hash['to_code'] 
        # it is a viceversa call.
        cross_category_id = self.get_cross_category_id_by_from_to_codes(category_code_a, category_code_b)
        if cross_category_id:
            # update status, first get record from DB
            data_tuple = self.get_cross_category_data_by_id(cross_category_id)
            self.id                 = data_tuple[0]
            self.from_category_id   = data_tuple[1] 
            self.from_code          = data_tuple[2]
            self.to_category_id     = data_tuple[3]
            self.to_code            = data_tuple[4]
            self.content_cross_reference    = data_tuple[5] 
            self.explicit_cross_reference   = data_tuple[6] 
            self.pending            = data_tuple[7]
            # then evaluate new values for cross category
            if self.to_category_id is None:
                # doesn't exist to category ID, but cross reference already exists. Update
                # from_category_id always exists by creation. (by content or explicit creation)
                ''' OLD routine:
                self.to_category_id = self.get_category_id_by_code(self.to_code)
                from_category_url = self.get_category_url_by_id(self.from_category_id)
                to_category_url = self.get_category_url_by_id(self.to_category_id)
                if from_category_url == to_category_url:
                    self.content_cross_reference = True
                self.pending = False 
                self.update_in_database()
                '''
                # pprint(data_tuple)
                # print('to category id is None')
                # from category, to category and pending resolution. 
                if self.to_code == data_hash['from_code']:
                    self.to_category_id = data_hash['from_category_id']
                    self.pending        = False
                else: 
                    # self.from_code == data_hash['from_code']
                    # Logically, this routine should never be executed.
                    # Why? it'd mean that is the second time a category in file is being defined.
                    # not the way back.
                    self.to_category_id = data_hash['to_category_id']
                    self.pending        = False
                # cross reference type resolution
                if self.content_cross_reference is False:
                    self.content_cross_reference = data_hash['content_cross_reference']
                if self.explicit_cross_reference is False:
                    self.explicit_cross_reference = data_hash['explicit_cross_reference']                    
            else:
                if self.pending: raise Exception("Wrong data in cross category.")

            self.update_in_database()
        else:
            # create cross_category in database
            self.from_category_id = data_hash['from_category_id']
            self.from_code = data_hash['from_code']
            self.to_category_id = data_hash['to_category_id']
            self.to_code = data_hash['to_code']
            self.content_cross_reference = data_hash['content_cross_reference'] 
            self.explicit_cross_reference = data_hash['explicit_cross_reference'] 
            self.pending = data_hash['pending']
            self.id = self.save_in_database()


    def data_to_dict(self):
        node_data = {
            'from_category_id'  : self.from_category_id, # string
            'from_code'         : self.from_code, # string 
            'to_category_id'    : self.to_category_id, # string
            'to_code'           : self.to_code, # string 
            'content_cross_reference': self.content_cross_reference, # string 
            'explicit_cross_reference': self.explicit_cross_reference, # string 
            'pending'           : self.pending, # string 
        }
        return node_data

    #private
    def save_in_database(self):
        data = self.data_to_dict()
        print(c['blue']+"*** creating cross reference ***"+c['end'])
        fields = self.dictionary_steamroller(data.keys())
        values = data.values()
        array_values = []
        for v in values: array_values.append(v)
        self.register_cross_category_in_table(fields, array_values)
        self.print_data()
        return self.get_cross_category_id_by_from_to_codes(self.from_code, self.to_code)


    # private
    def update_in_database(self):
        data = self.data_to_dict()
        print(c['blue']+"*** updating cross reference ***"+c['end'])
        self.update_cross_category_data(data, self.id)
        self.print_data()


    def print_data(self):
        print(c['blue']+"--------------------------------------"+c['end'])
        print(c['blue']+"         cross category data:         "+c['end'])
        print(c['blue']+"--------------------------------------"+c['end'])
        print(c['blue']+"*"+c['end']+" from_code: "+str(self.from_code))
        print(c['blue']+"*"+c['end']+" to_code: "+str(self.to_code))
        if self.pending:
            print(c['red']+"*"+c['end']+" pending: "+c['red']+str(self.pending)+c['end'])
        else:
            print(c['blue']+"*"+c['end']+" pending: "+c['blue']+str(self.pending)+c['end'])
        print(c['blue']+"--------------------------------------"+c['end'])